package level.editor.team.communication.messages;

/**
 * Klasse implementiert eine Message vom Client zum Server, welche gesendet wird, sobald ein Level
 * vom Spieler beendet (gewonnen, verloren oder abgebrochen) wird. Die LevelPlayedMessage beinhaltet
 * den Spielernamen, den Level-Namen, die für das Level benötigte Zeit und den Erfolg bzw.
 * Misserfolg.
 */
public class LevelPlayedMessage implements Message {

  private final String playerName;
  private final String levelName;
  private final double time;
  private final boolean success;

  /**
   * Der Konstruktor.
   *
   * @param playerName der Name des Spielers
   * @param levelName der Name des Level
   * @param time die für das Spiel benötigte Zeit
   * @param success boolean über Erfolges oder Misserfolg
   */
  public LevelPlayedMessage(String playerName, String levelName, double time, boolean success) {
    this.levelName = levelName;
    this.playerName = playerName;
    this.success = success;
    this.time = time;
  }

  /**
   * Liefert den Namen des Spielers.
   *
   * @return den Player-Namen
   */
  public String getPlayerName() {
    return playerName;
  }

  /**
   * Liefert den Namen des Level.
   *
   * @return den Level-Namen
   */
  public String getLevelName() {
    return levelName;
  }

  /**
   * Liefert die benötigte Zeit.
   *
   * @return die Zeit
   */
  public double getTime() {
    return time;
  }

  /**
   * Liefert einen Boolean, ob das Spiel gewonnen oder Verloren wurde.
   *
   * @return Erfolg oder Misserfolg des Spiels
   */
  public boolean successful() {
    return success;
  }
}
