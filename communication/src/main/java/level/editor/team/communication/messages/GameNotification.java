package level.editor.team.communication.messages;

import java.util.List;

/**
 * Klasse implementiert eine Message vom Server zu dem Client, der sich soeben mit dem Server
 * verbunden hat. Die GameNotification beinhaltet den erfolgreichen Verbindungsaufbau, so dass der
 * MenuScreen gestartet werden kann. Zudem beinhaltet es eine Liste aus allen verfügbaren Level. Sie
 * wird ebenfalls versendet, wenn der Spieler aus dem Editor in das Menü wechselt.
 */
public class GameNotification implements Message {
  private final String playerName;
  private final boolean isConnected;
  private final List<String> levelNamen;

  /**
   * Initialize the notification of the Player who joined the game.
   *
   * @param playerName den Namen des Spielers
   * @param isConnected einen boolean über den Verbindungsstatus
   * @param levelNamen eine Liste der verfügbaren Level
   */
  public GameNotification(String playerName, boolean isConnected, List<String> levelNamen) {
    this.playerName = playerName;
    this.isConnected = isConnected;
    this.levelNamen = levelNamen;
  }

  /**
   * Liefert den Namen des Spielers.
   *
   * @return den Spieler-Namen
   */
  public String getPlayerName() {
    return this.playerName;
  }

  /**
   * Liefert den Verbindungsstatus des Spielers.
   *
   * @return einen boolean über den Verbindungsstatus
   */
  public boolean isConnected() {
    return this.isConnected;
  }

  /**
   * Liefert die Liste der Namen aller verfügbaren Level.
   *
   * @return die Liste
   */
  public List<String> getLevelNamen() {
    return levelNamen;
  }
}
