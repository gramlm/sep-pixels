package level.editor.team.communication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import level.editor.team.communication.messages.Message;

/**
 * Klasse enthält einen Socket und sendet darauf. Die Klasse dient als Schnittstelle für die Client-
 * Server-Verbindung.
 *
 * <p>In Anlehnung an die Klasse Communication#Connection von Task 3-solution. Dies gilt für die
 * gesamte Klasse. https://uni2work.ifi.lmu.de/course/W21/IfI/PEEGS-LNQ/file
 */
public final class Connection implements AutoCloseable {

  private final PrintStream writer;
  private final BufferedReader reader;
  private final MoshiForMessage moshiForMessage;

  /**
   * Erzeugt die benötigten Reader/Writer und Moshi-Adapter für die Client-Serverconnection.
   *
   * <p>@param outputStream Outputstream
   *
   * <p>@param inputStream Inputstream
   *
   * <p>@throws IOException IOException falls Verbindungsfehler auftreten
   */
  public Connection(OutputStream outputStream, InputStream inputStream) throws IOException {
    writer = new PrintStream(outputStream, true, StandardCharsets.UTF_8);
    reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
    moshiForMessage = new MoshiForMessage();
  }

  /**
   * Liest die Message vom Socket.
   *
   * @return Die Message
   * @throws IOException falls Verbindungsfehler auftreten
   */
  public Message read() throws IOException {
    synchronized (reader) {
      String readString = reader.readLine();
      if (readString == null) {
        throw new IOException("Message kann nicht gelesen werden.");
      }
      return moshiForMessage.decryptToMessage(readString);
    }
  }

  /**
   * Schreibt die Message.
   *
   * @param message Die Message
   */
  public void write(Message message) throws IOException {
    synchronized (writer) {
      writer.println(moshiForMessage.encryptToString(message));
      writer.flush();
    }
  }

  @Override
  public void close() {
    try {
      writer.close();
      reader.close();
    } catch (IOException e) {
      System.out.println("Die Verbindung kann nicht geschlossen werden.");
      e.printStackTrace();
    }
  }
}
