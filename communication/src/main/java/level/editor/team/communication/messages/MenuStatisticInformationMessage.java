package level.editor.team.communication.messages;

import java.util.List;
import java.util.Map;

/**
 * Klasse implementiert eine Message vom Server zu dem Client, der gerade den MenuScreen aufgerufen
 * hat. Die MenuStatisticInformationMessage beinhaltet die verfügbaren Level mit zugehöriger
 * Statistik zur Darstellung im MenuScreen.
 */
public class MenuStatisticInformationMessage implements Message {

  private Map<String, List<String>> statisticMap;

  public MenuStatisticInformationMessage(Map<String, List<String>> statisticMap) {
    this.statisticMap = statisticMap;
  }

  /**
   * Liefert die Map, welche die Statistik enthält.
   *
   * @return Die Statistik-Map
   */
  public Map<String, List<String>> getMap() {
    return statisticMap;
  }
}
