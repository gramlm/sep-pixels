package level.editor.team.communication;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.adapters.PolymorphicJsonAdapterFactory;
import java.io.IOException;
import level.editor.team.communication.messages.GameNotification;
import level.editor.team.communication.messages.JoinGameRequest;
import level.editor.team.communication.messages.LevelPlayedMessage;
import level.editor.team.communication.messages.LevelRequest;
import level.editor.team.communication.messages.MenuStatisticInformationMessage;
import level.editor.team.communication.messages.Message;
import level.editor.team.communication.messages.TransferLevel;

/**
 * Klasse dient zur Codierung und Decodierung von Messages in Json-String bzw. umgekehrt. Sie
 * enthält einen Json-Adapter und verwendet das Format Moshi.
 *
 * <p>In Anlehnung an die Klasse MessageCoder von Task 3-solution. Dies gilt für die gesamte Klasse.
 * https://uni2work.ifi.lmu.de/course/W21/IfI/PEEGS-LNQ/file
 */
public final class MoshiForMessage {

  private final JsonAdapter<Message> adapter;

  /** Konstruktor. */
  public MoshiForMessage() {

    // Moshi is used as the communication protocol.
    Moshi moshi =
        new Moshi.Builder()
            .add(
                PolymorphicJsonAdapterFactory.of(Message.class, "messageType")
                    .withSubtype(GameNotification.class, "GameNotification")
                    .withSubtype(JoinGameRequest.class, "JoinGameRequest")
                    .withSubtype(LevelPlayedMessage.class, "LevelPlayedMessage")
                    .withSubtype(LevelRequest.class, "LevelRequest")
                    .withSubtype(
                        MenuStatisticInformationMessage.class, "MenuStatisticInformationMessage")
                    .withSubtype(TransferLevel.class, "TransferLevel"))
            .build();
    adapter = moshi.adapter(Message.class);
  }

  /*
   * Encrypt Json-Message to string.
   */
  public String encryptToString(Message message) {
    return (new MoshiForMessage()).adapter.toJson(message);
  }

  /*
   * Decrypt string to Json-Message.
   */
  public Message decryptToMessage(String jsonString) throws IOException {
    return new MoshiForMessage().adapter.fromJson(jsonString);
  }
}
