package level.editor.team.communication.messages;

import java.util.List;

/**
 * Klasse implementiert eine Message vom Server zum Client und umgekehrt. Die LevelTransfer
 * beinhaltet ein Level als String.
 */
public class TransferLevel implements Message {

  private final String levelName;
  private final List<String> level;

  public TransferLevel(String levelName, List<String> level) {
    this.levelName = levelName;
    this.level = level;
  }

  /**
   * Liefert den Namen des Level als String.
   *
   * @return den Namen des Level
   */
  public String getLevelName() {
    return this.levelName;
  }

  /**
   * Liefert eine Liste aller verfügbaren Level.
   *
   * @return die Liste
   */
  public List<String> getLevel() {
    return this.level;
  }
}
