package level.editor.team.communication.messages;

/**
 * Klasse implementiert eine Message vom Client zum Server. Die LevelRequest beinhaltet den Namen
 * des Level, welches vom Spieler ausgewählt wurde.
 *
 * <p>In Anlehnung an die Klasse JoinGameRequest von Task 3-solution.
 */
public class LevelRequest implements Message {

  private final String playerName;
  private final int levelNumber;

  public LevelRequest(String playerName, int levelNumber) {
    this.levelNumber = levelNumber;
    this.playerName = playerName;
  }

  /**
   * Liefert den Namen des Players.
   *
   * @return den Player-Namen
   */
  public String getPlayerName() {
    return this.playerName;
  }

  /**
   * Liefert die Level-Nummer.
   *
   * @return Integer der Level-Nummer
   */
  public int getLevelNumber() {
    return levelNumber;
  }
}
