package level.editor.team.communication.messages;

/**
 * Klasse implementiert eine Message vom Client zum Server. Die JoinGameRequest beinhaltet den
 * Spielernamen.
 *
 * <p>In Anlehnung an die Klasse JoinGameRequest von Task 3-solution. Dies gilt für die gesamte
 * Klasse. https://uni2work.ifi.lmu.de/course/W21/IfI/PEEGS-LNQ/file
 */
public class JoinGameRequest implements Message {

  private final String playerName;

  /**
   * Initialisiert den Spielernamen.
   *
   * @param playerName der Spielername
   */
  public JoinGameRequest(String playerName) {
    this.playerName = playerName;
  }

  /**
   * Liefert den Namen des Spielers.
   *
   * @return den Spielernamen
   */
  public String getPlayerName() {
    return this.playerName;
  }
}
