package level.editor.team.server.servercommunication;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import level.editor.team.communication.Connection;
import level.editor.team.communication.messages.GameNotification;
import level.editor.team.communication.messages.LevelPlayedMessage;
import level.editor.team.communication.messages.LevelRequest;
import level.editor.team.communication.messages.MenuStatisticInformationMessage;
import level.editor.team.communication.messages.Message;
import level.editor.team.communication.messages.TransferLevel;
import level.editor.team.server.serverlevelandstatistics.LevelManager;
import level.editor.team.server.serverlevelandstatistics.Statistics;

/**
 * Klasse ist zuständig für die Kommunikation mit einem Spieler. Diese Klasse repräsentiert einen
 * Spieler und tauscht Informationen mit dem Spiel aus.
 *
 * <p>In Anlehnung an die Klasse PlayerConnection von Task 3-solution. Dies gilt für die gesamte
 * Klasse. https://uni2work.ifi.lmu.de/course/W21/IfI/PEEGS-LNQ/file
 */
public class PlayerConnection implements Closeable {

  private final String playerName;
  private Connection connection;
  private LevelManager levelManager;
  private Statistics statistics;

  /**
   * Der Konstruktor um einen neuen Spieler entgegenzunehmen.
   *
   * @param playerName Name des Spielers.
   * @param connection Die connectionklasse der Nachrichten.
   */
  public PlayerConnection(String playerName, Connection connection) {
    this.playerName = playerName;
    this.connection = connection;
    levelManager = LevelManager.getInstance();
    statistics = Statistics.getInstance();
    List<String> fileNames = levelManager.getFileNames();
    for (String file : fileNames) {
      statistics.newLevel(file);
    }
  }

  /**
   * Die PlayerConnection reagiert auf ankommende Nachrichten.
   *
   * @throws IOException Gibt eine entstehende IOException weiter.
   */
  void reactToClient() throws IOException {
    Message message = connection.read();

    if (message instanceof LevelPlayedMessage) {
      // Die Statistic wird erneuert.
      statistics.changeLevelStatistic(message);

      MenuStatisticInformationMessage menuStatisticInformationMessage =
          new MenuStatisticInformationMessage(statistics.getLevelStatistic());
      connection.write(menuStatisticInformationMessage);

    } else if (message instanceof LevelRequest) {
      if (levelManager.getLevelName(((LevelRequest) message).getLevelNumber()) != null) {
        send(
            new TransferLevel(
                levelManager.getLevelName(((LevelRequest) message).getLevelNumber()),
                levelManager.readLevel(((LevelRequest) message).getLevelNumber())));
      } else {
        System.out.println("Angeforderte Level existiert nicht.");
      }
    } else if (message instanceof TransferLevel) {

      String fileName = "Level" + (levelManager.getFileNames().size() + 1) + ".txt";
      // Der LevelManager speichert das Level.
      levelManager.writeLevel(((TransferLevel) message).getLevel());

      // Die Statistic legt ein neues Verzeichnis für das neue Level an.
      statistics.newLevel(fileName);

      // Nach dem Erhalt eines neuen Levels wird die eine GameNotification gesendet, damit der
      // Spieler das neu erstellte Level auch auswählen kann.

      List<String> levelNames = levelManager.getFileNames();
      Message gameNotification = new GameNotification(playerName, true, levelNames);
      connection.write(gameNotification);

      // Zudem wird eine MenuStatisticInformationMessage gesendet, damit sie im MenuSreen angezeigt
      // werden kann.

      MenuStatisticInformationMessage menuStatisticInformationMessage =
          new MenuStatisticInformationMessage(statistics.getLevelStatistic());
      connection.write(menuStatisticInformationMessage);

    } else {
      System.out.println("Unbekannter Nachrichtentyp");
      ;
    }
  }

  /**
   * Sends a message to a client. adjusted from peegs_task_3_solution highlowcardgame
   *
   * @param message The message to write
   */
  private void send(Message message) throws IOException {
    if (connection != null) {
      connection.write(message);
    }
  }

  /**
   * Getter des Spielernamens.
   *
   * @return den Spielernamen.
   */
  public String getPlayerName() {
    return playerName;
  }

  @Override
  public void close() {
    connection.close();
    connection = null;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PlayerConnection that = (PlayerConnection) o;
    return Objects.equals(playerName, that.playerName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(playerName);
  }
}
