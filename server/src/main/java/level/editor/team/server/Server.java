package level.editor.team.server;

import java.net.ServerSocket;
import java.net.Socket;
import level.editor.team.server.servercommunication.ConnectionManager;
import level.editor.team.server.serverlevelandstatistics.LevelManager;
import level.editor.team.server.serverlevelandstatistics.Statistics;

/**
 * Hauptklasse für den Gameserver. Die Klasse startet den Server-Socket, erstellt für jede
 * eingehende Verbindung einen neuen Socket, übergibt diesen an eine neue @PlayerConnection, die
 * sofort zum @ConnectionManager hinzugefügt wird.
 *
 * <p>In Anlehnung an die Klasse Server von Task 3-solution. Dies gilt für die gesamte Klasse.
 * https://uni2work.ifi.lmu.de/course/W21/IfI/PEEGS-LNQ/file
 */
public class Server {

  private static final int DEFAULT_PORT = 4441;

  /**
   * Main-Methode für den Server.
   *
   * @param args Commandline arguments
   */
  public static void main(final String[] args) {

    int port = DEFAULT_PORT;

    try (ServerSocket socket = new ServerSocket(port)) {
      Server server = new Server();
      server.start(socket);
    } catch (Exception e) {
      System.out.println("Connection lost. Shutting down: " + e.getMessage());
    }
  }

  /**
   * Startmethode des Servers. Hier wird ein Socket angenommen und an den ConnectionManager weiter
   * gereicht.
   *
   * @param socket der ServerSocket des Servers
   * @throws Exception wirft eine IOException
   */
  public void start(ServerSocket socket) throws Exception {
    LevelManager levelManager = LevelManager.getInstance();
    Statistics statistics = Statistics.getInstance();

    try (ConnectionManager connectionManager = new ConnectionManager()) {
      while (true) {
        Socket connectionSocket = socket.accept();
        connectionManager.createNewPlayerConnection(connectionSocket);
      }
    }
  }
}
