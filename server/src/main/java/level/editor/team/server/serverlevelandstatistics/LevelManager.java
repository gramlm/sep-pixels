package level.editor.team.server.serverlevelandstatistics;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Diese Klasse beinhaltet die Level und managed das speichern und lesen weiterer Level. Die Klasse
 * ist als Singleton implementiert, damit nicht mehrere Dateien gleichzeitig geschrieben werden.
 */
public class LevelManager {

  private List<String> fileNames;
  private final String path;
  private static LevelManager instance;

  /**
   * Konstruktor des LevelManager. Hier werden bei Aufruf die ersten beiden Level im
   * Ressource-Ornder bespeichert.
   */
  private LevelManager(String path) {
    this.path = path;
    fileNames = new ArrayList<>();
    readFiles();
    if (fileNames.size() < 2) {
      createLevel1();
      createLevel2();
    }
  }

  /**
   * Instanz des LevelManagers.
   *
   * @return Instanz des LevelManagers.
   */
  public static synchronized LevelManager getInstance() {
    if (LevelManager.instance == null) {
      String path = "../server/src/main/resources";
      instance = new LevelManager(path);
    }
    return LevelManager.instance;
  }

  /**
   * Level als Moshi-String erzeugen, welches lediglich aus dem Start, dem Ausgang (Finish) und den
   * Blöcken in einer Reihe besteht. Ein Objekt besteht aus den Integerwerten der Koordinate (Zeile,
   * Spalte) und den Boolean-Werten für isStart, isFinish, isBlock.
   */
  public void createLevel1() {

    List<String> testLevel = new ArrayList<>();
    StringBuilder builder = new StringBuilder();
    String levelObject;

    // Start einfügen
    levelObject =
        builder
            .append(39)
            .append(" ")
            .append(7)
            .append(" ")
            .append(true)
            .append(" ")
            .append(false)
            .append(" ")
            .append(false)
            .toString();
    testLevel.add(levelObject);
    builder.delete(0, builder.length());

    // Finish einfügen
    levelObject =
        builder
            .append(39)
            .append(" ")
            .append(41)
            .append(" ")
            .append(false)
            .append(" ")
            .append(true)
            .append(" ")
            .append(false)
            .toString();
    testLevel.add(levelObject);
    builder.delete(0, builder.length());

    // Blöcke einfügen
    for (int i = 3; i < 45; i++) {
      levelObject =
          builder
              .append(40)
              .append(" ")
              .append(i)
              .append(" ")
              .append(false)
              .append(" ")
              .append(false)
              .append(" ")
              .append(true)
              .toString();
      testLevel.add(levelObject);
      builder.delete(0, builder.length());
    }
    writeLevel(testLevel);
  }

  /**
   * Level als Moshi-String erzeugen, welches lediglich aus dem Start, dem Ausgang (Finish) und den
   * Blöcken in einer Reihe besteht. Ein Objekt besteht aus den Integerwerten der Koordinate (Zeile,
   * Spalte) und den Boolean-Werten für isStart, isFinish, isBlock.
   */
  public void createLevel2() {

    List<String> testLevel = new ArrayList<>();
    StringBuilder builder = new StringBuilder();
    String levelObject;

    // Start einfügen
    levelObject =
        builder
            .append(39)
            .append(" ")
            .append(7)
            .append(" ")
            .append(true)
            .append(" ")
            .append(false)
            .append(" ")
            .append(false)
            .toString();
    testLevel.add(levelObject);
    builder.delete(0, builder.length());

    // Finish einfügen
    levelObject =
        builder
            .append(24)
            .append(" ")
            .append(47)
            .append(" ")
            .append(false)
            .append(" ")
            .append(true)
            .append(" ")
            .append(false)
            .toString();
    testLevel.add(levelObject);
    builder.delete(0, builder.length());

    // Blöcke einfügen
    for (int i = 3; i < 15; i++) {
      levelObject =
          builder
              .append(40)
              .append(" ")
              .append(i)
              .append(" ")
              .append(false)
              .append(" ")
              .append(false)
              .append(" ")
              .append(true)
              .toString();
      testLevel.add(levelObject);
      builder.delete(0, builder.length());
    }

    // Blöcke einfügen
    for (int i = 13; i < 25; i++) {
      levelObject =
          builder
              .append(35)
              .append(" ")
              .append(i)
              .append(" ")
              .append(false)
              .append(" ")
              .append(false)
              .append(" ")
              .append(true)
              .toString();
      testLevel.add(levelObject);
      builder.delete(0, builder.length());
    }

    // Blöcke einfügen
    for (int i = 27; i < 32; i++) {
      levelObject =
          builder
              .append(31)
              .append(" ")
              .append(i)
              .append(" ")
              .append(false)
              .append(" ")
              .append(false)
              .append(" ")
              .append(true)
              .toString();
      testLevel.add(levelObject);
      builder.delete(0, builder.length());
    }

    // Blöcke einfügen
    for (int i = 35; i < 40; i++) {
      levelObject =
          builder
              .append(27)
              .append(" ")
              .append(i)
              .append(" ")
              .append(false)
              .append(" ")
              .append(false)
              .append(" ")
              .append(true)
              .toString();
      testLevel.add(levelObject);
      builder.delete(0, builder.length());
    }

    // Blöcke einfügen
    for (int i = 40; i < 50; i++) {
      levelObject =
          builder
              .append(25)
              .append(" ")
              .append(i)
              .append(" ")
              .append(false)
              .append(" ")
              .append(false)
              .append(" ")
              .append(true)
              .toString();
      testLevel.add(levelObject);
      builder.delete(0, builder.length());
    }

    writeLevel(testLevel);
  }

  /**
   * Es wird ein angefragtes Level aus dem Ressource-Ordner ausgelesen und zurückgegeben.
   *
   * @param levelNumber Die Nummer des Levels.
   * @return Gibt das Level als Liste von Strings zurück.
   */
  public List<String> readLevel(int levelNumber) {

    readFiles();

    List<String> levelAsList = new ArrayList<>();

    BufferedReader bufferedReader;
    File inputfile = new File(path, fileNames.get(levelNumber - 1));

    try {
      bufferedReader = new BufferedReader(new FileReader(inputfile, StandardCharsets.UTF_8));
      try {
        String line;
        boolean possibleReading = true;
        // while (bufferedReader.ready() && bufferedReader.readLine() != null) {
        while (bufferedReader.ready() && possibleReading) {
          line = bufferedReader.readLine();
          if (line != null) {
            levelAsList.add(line);
          } else {
            possibleReading = false;
            System.out.println("Probleme beim Lesen des Level.");
          }

          // levelAsList.add(bufferedReader.readLine());
        }
      } catch (IOException e) {
        System.out.println("Problem with File occurred");
      }
    } catch (IOException e) {
      System.out.println("No existing file");
    }
    return levelAsList;
  }

  /** Liest die Namen aller Dateien im ressourses. */
  private void readFiles() {

    fileNames.clear();

    File folder = new File(path);

    for (File file : folder.listFiles()) {
      fileNames.add(file.getName());
    }
  }

  /**
   * Schreibt ein Level in den Ordner ressourses in ein .txt-Datei. Die Listenelemente werden in je
   * einer neuen Zeile gespeichert.
   *
   * @param newLevel das zu schreibende Level als Liste on Strings.
   */
  public synchronized void writeLevel(List<String> newLevel) {

    readFiles();

    String fileName = "Level" + (fileNames.size() + 1) + ".txt";

    try {
      File file = new File(path, fileName);
      BufferedWriter bw = new BufferedWriter(new FileWriter(file));
      for (String zeile : newLevel) {
        bw.write(zeile);
        bw.newLine();
      }
      bw.close();
    } catch (IOException e) {
      System.out.println("An error occurred.");
      e.printStackTrace();
    }
    readFiles();
  }

  /**
   * Gibt den LevelNamen eines Levels wieder.
   *
   * @param levelNumber Die Nummer des Levels.
   * @return Gibt den Namen des Levels als String wieder.
   */
  public String getLevelName(int levelNumber) {
    return fileNames.get(levelNumber - 1);
  }

  /**
   * Gibt die Dateinamen der Level wieder.
   *
   * @return Liste der verfügbaren txt der Level.
   */
  public List<String> getFileNames() {
    // Kopie der Liste machen!
    readFiles();
    return fileNames;
  }
}
