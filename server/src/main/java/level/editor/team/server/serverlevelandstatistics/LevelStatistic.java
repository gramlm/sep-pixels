package level.editor.team.server.serverlevelandstatistics;

/** Diese Klasse repräsentiert die Statistik für jedes Level. */
public class LevelStatistic {

  private final String levelName;
  private final double besteZeit;
  private final int versuche;
  private final int gelungeneVersuche;
  private final String spielerNameBesteZeit;

  /**
   * Privater Konstruktor der LevelStatistic.
   *
   * @param levelName Der LevelName
   * @param besteZeit Die Bestzeit des Levels
   * @param versuche Die Anzahl der insgesamt getätigten Versuche.
   * @param gelungeneVersuche Die Anzahl der gelungenen Versuche.
   * @param spielerNameBesteZeit Der Name des schnellsten Spielers.
   */
  private LevelStatistic(
      String levelName,
      double besteZeit,
      int versuche,
      int gelungeneVersuche,
      String spielerNameBesteZeit) {
    this.levelName = levelName;
    this.besteZeit = besteZeit;
    this.versuche = versuche;
    this.gelungeneVersuche = gelungeneVersuche;
    this.spielerNameBesteZeit = spielerNameBesteZeit;
  }

  /**
   * Statische Methode um eine neue Statistik eines Levels anzulegen.
   *
   * @param levelName Der LevelName
   * @return Die neue LevelStatistik.
   */
  public static LevelStatistic neueLevelStatistic(String levelName) {
    double besteZeit = Double.POSITIVE_INFINITY;
    int versuche = 0;
    int gelungeneVersuche = 0;
    String spielerNameBesteZeit = "---";
    return new LevelStatistic(
        levelName, besteZeit, versuche, gelungeneVersuche, spielerNameBesteZeit);
  }

  /**
   * Methode um die LevelStatistic zu verändern.
   *
   * @param besteZeit Die Bestzeit des Levels
   * @param versuche Die Anzahl der insgesamt getätigten Versuche.
   * @param gelungeneVersuche Die Anzahl der gelungenen Versuche.
   * @param spielerNameBesteZeit Der Name des schnellsten Spielers.
   * @return Die neue LevelStatistik.
   */
  public LevelStatistic veraendernDerLevelStatistik(
      double besteZeit, int versuche, int gelungeneVersuche, String spielerNameBesteZeit) {
    return new LevelStatistic(
        levelName, besteZeit, versuche, gelungeneVersuche, spielerNameBesteZeit);
  }

  /** Getter der BestZeit. @return die Bestzeit als Date. */
  public double getBesteZeit() {
    return besteZeit;
  }

  /** Getter der insgesamten Versuche. @return Versuche als int. */
  public int getVersuche() {
    return versuche;
  }

  /** Getter der erfolgreichen Versuche. @return Versuche als int. */
  public int getGelungeneVersuche() {
    return gelungeneVersuche;
  }

  /** Getter des SpielerNamen der die beste Zeit geschafft hat. @return SpielerName als String. */
  public String getSpielerNameBesteZeit() {
    return spielerNameBesteZeit;
  }
}
