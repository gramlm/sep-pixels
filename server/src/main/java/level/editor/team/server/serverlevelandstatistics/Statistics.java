package level.editor.team.server.serverlevelandstatistics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import level.editor.team.communication.messages.LevelPlayedMessage;
import level.editor.team.communication.messages.Message;

/**
 * Klasse verwaltet die statistischen Daten zu einem Level: Anzahl der Versuche und Anzahl der
 * Erfolge eines Spielers in einem Level, die Bestzeit des Spielers.
 */
public class Statistics {

  // Die HashMap speichert zu jedem Level (LevelNamen) eine LevelStatistic.
  private Map<String, LevelStatistic> allLevelStatistic;
  private static Statistics instance;

  /** Privater Konstruktor der Statistic. Initialisiert die Hashmap. */
  private Statistics() {
    allLevelStatistic = new HashMap<>();
  }

  /**
   * Gibt den Singelton der Statistic zurück. Erstellt ihn, falls er noch nicht initialisiert
   * wurde. @return den Singelton der Statistik.
   */
  public static synchronized Statistics getInstance() {
    if (Statistics.instance == null) {
      instance = new Statistics();
    }
    return Statistics.instance;
  }

  /** Legt die Statistic eines neuen Levels an. @param levelNamen der Name des neuen Levels. */
  public void newLevel(String levelNamen) {
    LevelStatistic levelStatistic = LevelStatistic.neueLevelStatistic(levelNamen);
    allLevelStatistic.put(levelNamen, levelStatistic);
  }

  /**
   * Verändert die aktuell abgelegte Statistik. @param message LevelPlayed Nachricht mit neuen
   * Werten.
   */
  public void changeLevelStatistic(Message message) {
    String levelNamen = ((LevelPlayedMessage) message).getLevelName();
    int neueVersuche = allLevelStatistic.get(levelNamen).getVersuche() + 1;
    int gelungeneVersuche = allLevelStatistic.get(levelNamen).getGelungeneVersuche();
    if (((LevelPlayedMessage) message).successful()) {
      gelungeneVersuche++;
    }
    double besteZeit;
    String besterSpieler;
    if (((LevelPlayedMessage) message).getTime() < allLevelStatistic.get(levelNamen).getBesteZeit()
        && ((LevelPlayedMessage) message).successful()) {
      besteZeit = ((LevelPlayedMessage) message).getTime();
      besterSpieler = ((LevelPlayedMessage) message).getPlayerName();
    } else {
      besteZeit = allLevelStatistic.get(levelNamen).getBesteZeit();
      besterSpieler = allLevelStatistic.get(levelNamen).getSpielerNameBesteZeit();
    }
    LevelStatistic neueLevelStatistic =
        allLevelStatistic
            .get(levelNamen)
            .veraendernDerLevelStatistik(besteZeit, neueVersuche, gelungeneVersuche, besterSpieler);
    allLevelStatistic.remove(levelNamen);
    allLevelStatistic.put(levelNamen, neueLevelStatistic);
  }

  /**
   * Getter der Levelstatistik. Die Daten werden vorher in Strings umgewandelt, um besser Verschickt
   * und angezeigt zu werden. @return eine Map mit dem Levelnamen als Key und die Statistik als
   * Liste von Strings.
   */
  public Map<String, List<String>> getLevelStatistic() {
    Set<String> levelNamenSet = allLevelStatistic.keySet();
    List<String> levelNamen = new ArrayList<>();
    for (String level : levelNamenSet) {
      levelNamen.add(level);
    }
    Map<String, List<String>> cloneAllLevelStatistics = new HashMap<>();
    for (String levelName : levelNamen) {
      List<String> statisticAsString = new ArrayList<>();
      LevelStatistic levelstatistic = allLevelStatistic.get(levelName);
      // Alle Inhalte in Strings umwandeln.
      String besteZeit = String.valueOf(levelstatistic.getBesteZeit());
      String gelungeneVersuche = String.valueOf(levelstatistic.getGelungeneVersuche());
      String versuche = String.valueOf(levelstatistic.getVersuche());
      String besteSpieler = levelstatistic.getSpielerNameBesteZeit();
      // Der Liste hinzufügen.
      statisticAsString.add(besteZeit);
      statisticAsString.add(gelungeneVersuche);
      statisticAsString.add(versuche);
      statisticAsString.add(besteSpieler);
      // Der Map Hinzufügen.
      cloneAllLevelStatistics.put(levelName, statisticAsString);
    }
    return cloneAllLevelStatistics;
  }
}
