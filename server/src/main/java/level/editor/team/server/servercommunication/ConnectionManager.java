package level.editor.team.server.servercommunication;

import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import level.editor.team.communication.Connection;
import level.editor.team.communication.messages.GameNotification;
import level.editor.team.communication.messages.JoinGameRequest;
import level.editor.team.communication.messages.MenuStatisticInformationMessage;
import level.editor.team.communication.messages.Message;
import level.editor.team.server.serverlevelandstatistics.LevelManager;
import level.editor.team.server.serverlevelandstatistics.Statistics;

/**
 * Klasse beinhaltet eine HashMap von PlayerConnections, behandelt den JoinGameRequest und alle
 * Client-Messages.
 *
 * <p>In Anlehnung an die Klasse ConnectionManager von Task 3-solution. Dies gilt für die gesamte
 * Klasse. https://uni2work.ifi.lmu.de/course/W21/IfI/PEEGS-LNQ/file
 */
public class ConnectionManager implements AutoCloseable {

  private Map<String, PlayerConnection> connections = new HashMap<>();

  /** Konstruktor des ConnectionManagers. */
  public ConnectionManager() {}

  /**
   * Nimmt den neuen Socket an und kreiert eine PlayerConnection.
   *
   * @param connectionSocket socket zum neuen client.
   * @throws IOException Exception wird geworfen, wenn der socket geschlossen wird.
   */
  public void createNewPlayerConnection(Socket connectionSocket) throws IOException {
    Connection connection =
        new Connection(connectionSocket.getOutputStream(), connectionSocket.getInputStream());
    PlayerConnection player = handleJoinGameRequest(connection);
    connections.put(player.getPlayerName(), player);
    new Thread(() -> listenForInput(player)).start();
  }

  /**
   * Nimmt Messages vom Client entgegen und leitet sie an den entsprechenden Player des Clients
   * weiter.
   *
   * @param player der Spieler einer Clientverbindung.
   */
  private void listenForInput(PlayerConnection player) {
    try {
      while (true) {
        player.reactToClient();
      }
    } catch (IOException e) {
      System.err.println(
          "IOException on " + player.getPlayerName() + ", disconnecting: " + e.getMessage());
      if (connections != null) {
        connections.remove(player.getPlayerName());
      }
    }
  }

  /**
   * Behandelt die JoinGameRequest Nachricht vom Client.
   *
   * @param connection Connection-Klasse der Messages
   * @return die neue PlayerConnection
   * @throws IOException Wirft Exception falls die Connection nichts lesen kann.
   */
  private PlayerConnection handleJoinGameRequest(Connection connection) throws IOException {
    // Initially expect a message from the clients to know what they want.
    Message registrationMessage = connection.read();
    if (!(registrationMessage instanceof JoinGameRequest)) {
      connection.close();
      throw new AssertionError("Unknown Message type!");
    }
    String playerName = ((JoinGameRequest) registrationMessage).getPlayerName();

    // Der LevelManager sendet die aktuellen Level.
    LevelManager levelManager = LevelManager.getInstance();
    List<String> levelNames = levelManager.getFileNames();
    Message joinGameNotification = new GameNotification(playerName, true, levelNames);
    connection.write(joinGameNotification);

    // Die Statistic sendet die aktuelle Statistic dazu. Dafür wird die Statistic initialisiert und
    // zu allen aktuell abgespeicherten Level wird eine Statistik erstellt.

    Statistics statistics = Statistics.getInstance();
    if (statistics.getLevelStatistic().isEmpty()) {
      for (String level : levelNames) {
        statistics.newLevel(level);
      }
    }
    Map<String, List<String>> levelStatistic = statistics.getLevelStatistic();
    MenuStatisticInformationMessage menuStatisticInformationMessage =
        new MenuStatisticInformationMessage(levelStatistic);
    connection.write(menuStatisticInformationMessage);

    return new PlayerConnection(playerName, connection);
  }

  @Override
  public void close() {
    for (PlayerConnection player : connections.values()) {
      player.close();
    }
    connections = null; // so that it can't be used anymore
  }
}
