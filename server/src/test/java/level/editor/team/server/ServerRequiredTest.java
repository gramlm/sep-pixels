package level.editor.team.server;

import static com.google.common.truth.Truth.assertThat;
import static level.editor.team.server.TestUtils.getNetworkIn;
import static level.editor.team.server.TestUtils.getNetworkOut;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import level.editor.team.server.serverlevelandstatistics.LevelManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

/**
 * Eine Klasse um den Server zu testen.
 *
 * <p>In Anlehnung an die Klasse ConnectionManager von Task 3-solution. Dies gilt für die gesamte
 * Klasse. https://uni2work.ifi.lmu.de/course/W21/IfI/PEEGS-LNQ/file
 */
@Timeout(7)
public class ServerRequiredTest {

  private static final String USER1 = "TestPlayer";

  @Test
  public void testServer1_receivesJoinGame_sendsGameNotification()
      throws IOException, InterruptedException {
    String joinRequest = "{\"messageType\":\"JoinGameRequest\",\"playerName\":\"" + USER1 + "\"}";
    MockInputStream networkIn = getNetworkIn(joinRequest);
    ByteArrayOutputStream networkOut = getNetworkOut();
    MockSocket mockSocket = new MockSocket(networkIn, networkOut);
    MockServerSocket serverSocket = new MockServerSocket(List.of(mockSocket));

    LevelManager levelManager = LevelManager.getInstance();

    Thread.sleep(100);

    TestUtils.startServer(serverSocket);
    do {
      Thread.sleep(10);
    } while (!networkIn.isDone());
    Thread.sleep(Sleeps.SLEEP_BEFORE_TESTING);

    String sent = networkOut.toString(StandardCharsets.UTF_8);
    String[] jsonMessages = sent.split(System.lineSeparator());
    for (String message : jsonMessages) {
      if (message.matches(".*\"messageType\"\\s*:\\s*\"GameNotification\".*")
          && message.matches(".*\"playerName\"\\s*:\\s*\"" + USER1 + "\".*")) {
        assertThat(message).matches(".*\"isConnected\"\\s*:\\s*true.*");
        // assertThatContainsNKeyValuePairs(message, 3);
        return;
      }
    }
    Assertions.fail("No GameNotification for player " + USER1);
  }
}
