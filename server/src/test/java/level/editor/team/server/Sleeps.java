package level.editor.team.server;

/**
 * Eine Klasse um den Server zu testen. Übernommen aus der Lösung von Task-3 HighLowCardGame.
 * https://uni2work.ifi.lmu.de/course/W21/IfI/PEEGS-LNQ/file
 */
public final class Sleeps {

  static final int SLEEP_BETWEEN_SOCKET_ACCEPTS = 100 /* ms */;
  static final int SLEEP_BEFORE_TESTING = 1000 /* ms */;
}
