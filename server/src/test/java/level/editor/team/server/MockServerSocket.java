package level.editor.team.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Iterator;
import java.util.List;

/**
 * Eine Klasse um den Server zu testen. Übernommen aus der Lösung von Task-3 HighLowCardGame.
 * https://uni2work.ifi.lmu.de/course/W21/IfI/PEEGS-LNQ/file
 */
public class MockServerSocket extends ServerSocket {

  private boolean isClosed = false;

  private Iterator<level.editor.team.server.MockSocket> sockets;

  public MockServerSocket(List<level.editor.team.server.MockSocket> socketsToAccept)
      throws IOException {
    sockets = socketsToAccept.iterator();
  }

  @Override
  public Socket accept() {
    if (sockets.hasNext()) {
      try {
        Thread.sleep(level.editor.team.server.Sleeps.SLEEP_BETWEEN_SOCKET_ACCEPTS);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      return sockets.next();
    }
    synchronized (this) {
      try {
        wait();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    return null;
  }

  public void waitOnFinished() throws InterruptedException {
    synchronized (this) {
      wait();
    }
  }

  public void finish() {
    synchronized (this) {
      notifyAll();
    }
  }

  @Override
  public void close() {
    synchronized (this) {
      notifyAll();
    }
    isClosed = true;
  }

  @Override
  public boolean isClosed() {
    return isClosed;
  }
}
