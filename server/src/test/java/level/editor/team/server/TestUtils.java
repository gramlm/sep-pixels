package level.editor.team.server;

import java.io.ByteArrayOutputStream;
import java.net.ServerSocket;
import java.util.Collection;
import java.util.NoSuchElementException;

/**
 * Eine Klasse um den Server zu testen. Übernommen aus der Lösung von Task-3 HighLowCardGame.
 * https://uni2work.ifi.lmu.de/course/W21/IfI/PEEGS-LNQ/file
 */
public final class TestUtils {
  static Server startServer(ServerSocket serverSocket) {
    Server server = new Server();
    new Thread(
            () -> {
              try {
                server.start(serverSocket);
              } catch (NoSuchElementException e) {
                // expected, because number of incoming clients will be exhausted quickly.
              } catch (Exception e) {
                e.printStackTrace();
              }
            })
        .start();
    return server;
  }

  static level.editor.team.server.MockInputStream getNetworkIn(String input) {
    return new level.editor.team.server.MockInputStream(input + System.lineSeparator());
  }

  static ByteArrayOutputStream getNetworkOut() {
    return new ByteArrayOutputStream();
  }

  static boolean areDone(Collection<level.editor.team.server.MockInputStream> inputs) {
    return inputs.stream().allMatch(level.editor.team.server.MockInputStream::isDone);
  }
}
