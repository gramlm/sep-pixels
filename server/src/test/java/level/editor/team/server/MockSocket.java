package level.editor.team.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketAddress;

/**
 * Eine Klasse um den Server zu testen. Übernommen aus der Lösung von Task-3 HighLowCardGame.
 * https://uni2work.ifi.lmu.de/course/W21/IfI/PEEGS-LNQ/file
 */
class MockSocket extends Socket {

  private final InputStream input;
  private final OutputStream output;
  private boolean isClosed = false;

  MockSocket(InputStream input, OutputStream output) {
    this.input = input;
    this.output = output;
  }

  @Override
  public void connect(SocketAddress endpoint) {}

  @Override
  public void connect(SocketAddress endpoint, int timeout) {}

  @Override
  public InputStream getInputStream() throws IOException {
    return input;
  }

  @Override
  public OutputStream getOutputStream() throws IOException {
    return output;
  }

  @Override
  public void close() {
    isClosed = true;
  }

  @Override
  public boolean isConnected() {
    return !isClosed;
  }

  @Override
  public boolean isBound() {
    return true;
  }

  @Override
  public boolean isClosed() {
    return isClosed;
  }
}
