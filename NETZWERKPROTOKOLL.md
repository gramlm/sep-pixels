Verändert nach https://www.sosy-lab.org/Teaching/2021-WS-SEP/3_socket.html

JSON Communication Protocol

We use JSON as the communication protocol, every message is encoded in JSON format.

Client to Server

    JoinGameRequest: the message that asks for joining the game.

    {"messageType":"JoinGameRequest","playerName":<NAME>}

        String <NAME>: the player’s name

    LevelPlayedMessage: the message containing the player’s scores after playing a level.

    {"messageType":"LevelPlayedMessage","playerName":<NAME>,"levelName":<LEVELNAME>,"time":<TIME>,"success":<SUCCESS>}

        String <NAME>: the player's name

        String <LEVELNAME>: the name of the played level

        double <TIME>: the time the player needed to end the game

        boolean <SUCCESS>: <TRUE> if the player won, <FALSE> if the player lost

    LevelRequest: the message that asks for a given level.

    {"messageType":"LevelRequest","playerName":<NAME>,"levelNumber":<LEVELNUMBER>}

        String <NAME>: the player’s name

        int <LEVELNUMBER>: the mumber of the wanted level from the list of level

    TransferLevel: the message sending the new level to the server.

    {"messageType":"TransferLevel","levelName":<LEVELNAME>,"level":<LEVEL>}

        String <LEVELNAME>: the name of the played level

        List<String> <LEVEL>: the new level as string

Sever to Client

    GameNotification: the message containing all playable level and if the server is online.

    {"messageType":"GameNotification","playerName":<NAME>,"isConnected":<CONNECTED>,"levelNamen":<LEVELNAMEN>}

        String <NAME>: the player's name

        boolean <SUCCESS>: <TRUE> if the server is online

        List<String> <LEVELNAMEN>: a list of all saved level on the server

    MenuStatistiscInformationMessage: the message given the statistic of all level.

    {"messageType":"MenuStatistiscInformationMessage","statisticMap":<STATISTICMAP>}

        Map<String, List<String>> <STATISTICMAP>: a map containing all levelname as key and a list of all needed statistic informations in a list build of strings

    TransferLevel: the message sending the requested level to the client.

    {"messageType":"TransferLevel","levelName":<LEVELNAME>,"level":<LEVEL>}

        String <LEVELNAME>: the name of the played level

        List<String> <LEVEL>: the requested level as string