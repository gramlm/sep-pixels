#

# _PixelRunner - by the Pixels_

# Jump'n'Run game

Hinweise zum Starten und Spielen von PixelRunner:

**Ausführen der GradleTasks:**

- manuelles Starten des Servers (server → Tasks → run)
- manuelles Starten des Client  (client → Tasks → run)

_WelcomeScreen_

- Eingabe des Spielernamens und Klicken des 'Start'-Buttons, um in den MenüScreen zu gelangen

_MenüScreen_

- Auswahl des Spiellevels im DropDown-Menü
- wenn das Level und eine der 3 Spielfiguren ausgewählt wurden, lässt sich das Spiel starten
- durch Anklicken des LevelEditor-Buttons öffnet sich der Editor zum Erstellen und Editieren von
  Levels anhand vorhandener Level-Vorlagen

_GameScreen_

- Steuerung des Players mittels der Cursor-Tasten _**⇦ links | rechts ⇨**_ und **␠ SPACE ␠** zum
  Ausführen der Sprungbewegung
- für die Seitwärtsbewegung muss die entsprechende Pfeiltaste wiederholt gedrückt werden, ein
  permanenter Tastendruck wird nicht registriert
- Ziel des Spiels: der Player muss vom Start (rot) zum Exit (grün) gelangen
- wenn der Player die roten Fire-Blöcke berührt oder den GameScreen im freien Fall verlässt, ist das
  Spiel verloren
- das aktuelle Level kann durch Anklicken von "nochmal" erneut gespielt werden
- Grundsätzlich muss langsam gespielt werden, damit es nicht zu Dastellungsproblemen kommt. Nach
  jeder Bewegung muss der Spieler warten, bis die Bewegung vollständig auf dem Bildschirm
  dargestellt worden ist.

_Level-Editor_

- die Level-Vorlagen werden durch den '>' Turnaround-Button geladen
- beim Anklicken von 'Level speichern' wird das Level sowohl an den Server gesendet als auch lokal
  im Editor gespeichert
- das abgespeicherte Level ist zur weiteren Änderung verfügbar, wenn man erneut vom MenüScreen aus
  den Level-Editor aufruft

  Ergänzende Hinweise:
- Je nach Bildschirmauflösung kommt es zu leicht unterschiedlichen Darstellungen der einzelnen
  Screens. Unter scratch_sketch findet sich eine pdf-Datei mit Abbildungen der Screens in der
  optimalen Darstellung.