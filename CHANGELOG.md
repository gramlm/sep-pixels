CHANGELOG.md according to https://keepachangelog.com/en/1.0.0/

How do I make a good changelog?

Guiding Principles

    Changelogs are for humans, not machines.
    There should be an entry for every single version.
    The same types of changes should be grouped.
    Versions and sections should be linkable.
    The latest version comes first.
    The release date of each version is displayed.
    Mention whether you follow Semantic Versioning.

Types of changes

    Added for new features.
    Changed for changes in existing functionality.
    Deprecated for soon-to-be removed features.
    Removed for now removed features.
    Fixed for any bug fixes.
    Security in case of vulnerabilities.

## [1.2.1] - 06.02.2022

### added by Eva

- PDF mit Abbildungen aller Sceens in scratch_sketch ergänzt. Dort kann man nun sehen, wie die
  Screens aussehen sollen – je nach Bildschirmauflösung werden sie unterschiedlich dargestellt.

## [1.2.0] - 05.02.2022

### added by Pamela

- Sprachauswahl ergänzt.

## [1.1.22] - 03.02.2022

### added by Angelina und Eva

- Zielbereich in GUI vergrößert
- Move-Methode wurde so abgeändert, dass man für jeden Schritt neu klicken muss.

## [1.1.21] - 02.02.2022

### added by Angelina

- Code aufräumen im package Communication angepasst.
- Javadoc ergänzt.

## [1.1.20] - 02.02.2022

### added by Volker

- Code aufräumen im LevelEditor, Client, README.md angepasst.
- nicht benötigte Klassen (vom ursprünglichen Gradle-Projekt) und zugehörige Tests entfernt.
- javadoc ergänzt (CheckStyle-Meldungen) und in view/action (GameMusicThreadExperiment).

## [1.1.19] - 31.01.2022

### added by Eva

- Code aufräumen im Client#model#board und Client#view angepasst.
- nicht benötigte Klassen entfernt.
- javadoc ergänzt.

## [1.1.18] - 31.01.2022

### added by Angelina und Eva

- Beim ersten Versuch ein Level zu spielen werden nie Gefahrenblöcke erzeugt, bei allen weiteren
  Versuchen per Zufallsgenerator.
- Nochmal-Spielen-Button im GameScreen ist implementiert.

## [1.1.17] - 29.01.2022

### added by Angelina und Eva

- Bug (LevelPlayedMessage wird zweimal gesendet) behoben.
- Bug beim Laden des Levels behoben.
- Nummer des Versuchs sowie die Bestzeit (falls Anzahl erfolgreiche Versuche >0, sonst "--") in
  Sekunden werden im GameScreen angezeigt.

## [1.1.16] - 26.01.2022

### added by Volker

- Bug im GameScreen entfernt: Levels können jetzt alle geladen werden.

## [1.1.15] - 26.01.2022

### added by Michael

- Bestzeit wurde auf --- gesetzt, falls noch keine Zeit angegeben wurde.
- Javadoc beim Server ergänzt.

## [1.1.14] - 26.01.2022

### added by Michael

- Bugfixes bei der Statistik

## [1.1.13] - 26.01.2022

### added by Angelina

- Bug im LevelManager beseitigt.

## [1.1.12] - 25.01.2022

### added by Michael

- Statistik wird im MenuScreen angezeigt.

## [1.1.11] - 24.01.2022

### added by Eva

- Neue FXML-Datei für den WelcomeScreen mit Check-Boxes für die Sprachauswahl erstellt.
- Sprunglevel in eine txt-Datei eingetragen (Level2.txt).

## [1.1.10] - 24.01.2022

### added by Angelina und Eva

- Musik wird während des Spiels abgespielt und wird beim Verlassen des GameScreens automatische
  beendet.
- im MenuScreen: Fehlermeldung, falls Spielfigur oder Level nicht ausgewählt wurden.
- im MenuScreen: ausgewählte Spielfigur wird markiert.
- actuellX und actuellY refactored zu actuelX und actuelY.

## [1.1.9] - 24.01.2022

### added by Pamela

- Stoppuhr wird immer aktualisiert.

## [1.1.8] - 24.01.2022

### added by Angelina und Eva

- Sprunglevel als txt-Datei unter server#resoureces integriert.

## [1.1.7] - 24.01.2022

### added by Angelina und Eva

- Zusatzfeature: Auswahl von verschiedenen Spielerfiguren im MenuScreen. Sie unterscheiden sich
  hinsichtlich der Lauf- und Sprunggeschwindigkeiten und in ihrer Farbe.

## [1.1.6] - 24.01.2022

### added by Volker

- Performance-Verbesserung des Editors.
- neue Funktionalität beim Setzen des Start- und Finish-Blocks: es werden automatisch 5 Blöcke
  gesetzt.
- erstellte Level werden beim Senden an den Server gesendet und lokal gespeichert.
- Level-Vorlagen wurden erstellt und hinzugefügt.

## [1.1.5] - 24.01.2022

### added by Volker

- README.md angepasst

## [1.1.4] - 22.01.2022

### added by Eva und Angelina

- Fenstergrößen der Screens fixiert.

## [1.1.3] - 20.01.2022

### added by Michael

- GameScreenControl sendet LevelPlayedMessage.
- GameScreenControl wird Spiel und Client übergeben.
- Client startet das Spiel GameScreenControl beendet das Spiel.

## [1.1.2] - 20.01.2022

### added by Volker

- Der LevelEditor kann nun wieder zurück zum MenuScreen

## [1.1.1] - 20.01.2022

### added by Angelina und Eva

- Zusatzfeature gefährliche Blöcke implementiert.
- Zufällig wird entschieden, ob ein Spiel gefährliche Blöcke enthält (Wahrscheinlichkeit 50%).
- Falls das Spiel gefährliche Blöcke enthalten soll, wird für jeden normalen Block mit einer Wk. von
  10% ein gefährlicher Block erzeugt, der in der Höhe 0, 1, 2, 3 oder 4 über dem normalen Block
  platziert wird.
- Berührt der Player einen gefährlichen Block, so ist das Spiel verloren.
- Ausnahme: Beim schrägen Sprung nach oben durchdringt der Player die gefährlichen Blöcke ebenso wie
  die normalen Blöcke problem- und gefahrlos.
- In Start und Ziel sind keine gefährlichen Blöcke.

## [1.1.0] - 19.01.2022

### added by Pamela

- Welcomescrene ist mit Menuscreen verbunden.
- Menuscreen ist mit GameScreen verbunden.
- Menuscreen ist mit LevelEditor verbunden.

## [1.0.25] - 17.01.2022

### added by Angelina und Eva

- GameScreenControl, WellcomeScreenControll, MenuScreenControll: Fenstergröße ändern wurde
  unterbunden.

## [1.0.24] - 16.01.2022

### added by Michael

- LevelStatistik ist angelegt.
- Statistics geschrieben und in PlayerConnection angebunden.
- MenuStatisticInformationMessage angelegt.

## [1.0.23] - 16.01.2022

### added by Angelina und Eva

- GameScreen: Der Player kann jetzt auch verlieren und gewinnen.
- Wenn das Spiel verloren ist, wird Game-Over auch im Screen angezeigt.
- Wenn das Spiel gewonnen ist, wird Gewonnen auch im Screen angezeigt.

## [1.0.22] - 15.01.2022

### added by Angelina und Eva

- GameScreen: Der Player kann sich in x-Richtung bewegen und springen (sowohl senkrechter Sprung als
  auch Parabelsprung bei Geschwindigkeit in x-Richtung). Er fällt, wenn er sich nicht auf einem
  Block befindet.
- Wenn das Spiel verloren ist, wird Game-Over erkannt.

## [1.0.21] - 11.01.2022

### added by Volker

- Editor-Funktionalität: Start, Ziel, Blöcke lassen sich setzen
- Level können in eine Level-Message konvertiert werden
- Zusatzfeature: Level-Editor-Music ist abspielbar (composed by: V. Arnheiter)

## [1.0.20] - 10.01.2022

### added by Michael

- GameNotification wurde aus ehemaliger JoinGameNotification erstellt.

## [1.0.19] - 07.01.2022

### added by Michael

- Alle vorhanden Dateien in ressources (server) werden ausgelesen.
- Die beiden Messages zum Versenden der Level wurden in eine Message zusammengefasst.

## [1.0.18] - 06.01.2022

### added by Volker

- LevelEditorControlTest.java erstellt
- unitTests in CI (gitlab-ci.yml) aktiviert

## [1.0.17] - 05.01.2022

### added by Michael

- LevelManager kann nun Level speichern und auslesen.
- PlayerConnection versendet auf LevelRequest das gespeicherte Level an den Client.
- Server-Client-Kommmunikation erfolgreich angelegt.
- LevelManager ist ein Singleton.

## [1.0.16] - 05.01.2022

### added by Michael

- gameState wurde rudimentär angelegt.
- jumpnRun wurde rudimentär angelegt.

## [1.0.15] - 05.01.2022

### added by Pamela and Michael

- ClientCommunication handleMessage implementiert.
- Server-Client-Kommmunikation erfolgreich an

## [1.0.14] - 05.01.2022

### added by Pamela

- ClientCommunication implementiert.
- StopUhr in Playtime implementiert.

## [1.0.13] - 03.01.2022

### added by Michael

- resources beim Server ergänzt.
- read-Methode im LevelManager erstellt.
- write-Methode im LevelManager erstellt.

## [1.0.12] - 04.01.2022

### added by Angelina

- TransferCreatedNewLevel wurde implementiert.
- mehrere Testlevel erzeugt und als Moshi-Message verschickt.
- LevelManager wurde ergänzt.

## [1.0.11] - 03.01.2022

### added by Angelina

- die TransferSelectedMessage wurde ergänzt.
- das model (Client, Level, ClientCommunication) wurde verändert, damit die TransferSelectedMessage
  funktioniert.
- die connection wurde ergänzt.

## [1.0.10] - 03.01.2022

### added by Michael

- Server, ConnectionManager, PlayerConnection und Connection (messages) wurden aus dem
  Task3-solution kopiert.
- LevelManager wurde im Server ergänzt, um die Level zu managen.

## [1.0.9] - 03.01.2022

### added by Eva

- Controller für die view wurden mit Methodenrümpfen erstellt.
- GameScreenControl wurde implementiert, ist noch nicht ganz fertig.
- die views (GameScreen, MenueScreen, WelcomeScreen) wurden erstellt.
- das model (Coordinate, Level, Pixel) wurde verändert, damit der GameScreenControl funktioniert.
- Die Klasse GameScreenControl wurde nicht committed, weil zu diesem Zeitpunkt die Sprungbewegung
  noch nicht funktioniert (Missverständis: ich hatte gedacht, es darf nur funtkionierender Code
  committed werden)

## [1.0.8] - 28.12.2021

### added by Angelina und Eva

- TestLevel in Server#PlayerConnection erstellt.
- Communication wurde überarbeitet und teilweise implementiert.
- Moshi-Adapter wurde erstellt.
- Implementierung der Klassen MoshiForMessage und JoinGameRequest

## [1.0.7] - 28.12.2021

### added by Volker

- Gradle-Projekt erweitert bzgl. Tasks, Checks, Artifacts
- Build-Task javaFxJar hinzugefügt (wird nur bei Merge in main-Branch erstellt), assemble-check (bei
  jedem commit)
- Checks/Tests: spotless, checkstyle & spotbugs (in Main-Klassen), spotbugs failed tests verhindern
  nicht den Merge in den Main-Branch
- artifacts aller build-, test-, javadoc-Jobs können als artifacts in gitlab heruntergeladen werden

## [1.0.6] - 22.12.2021

### merged by Michael

- Angelegte Klassen von Angelina und Eva wurden in den Main-Branche gemerged
- Struktur des Projekts wurde dadurch erstmal grob angelegt
- .idea wurde aus dem Projekt entfernt
- game-Projekt wurde aus dem Projekt in den client gezogen (Volker)
- CI Integration mit Basictest wurden integriert und lauffähig gemacht (Volker)

## [1.0.5] - 20.12.2021

### added by Angelina und Eva

- FXML-Dateien für Welcome- und Menu-Screen

## [1.0.4] - 19.12.2021

### added by Angelina und Eva

- Javadoc-Kommentare zu allen Klassen geschrieben
- Kommentare zu allen Methoden aller Klassen geschrieben

## [1.0.3] - 16.12.2021

### added by Volker

- Gradle Projekt in main-Branch gemerged
- im erstellten Gradle Projekt Unterprojekte erstellt
- Anpassungen in einzelnen .build-Dateien
- buildSrc bleibt vorerst, wurde erzeugt beim Erstellen des grundständigen Projekts
- buildSrc Funktion noch unklar, ggf. wird es bei größeren Projekten gebraucht
- server und client besitzen eine main-Methode und sind lauffähig

## [1.0.2] - 16.12.2021

### added by Angelina und Eva

- alle Klassen angelegt
- Methodenköpfe zu allen Klassen geschrieben

## [1.0.1] - 13.12.2021

### added by Angelina und Eva

- Festlegen der genauen Projektstruktur
- Festlegen aller Klassen
- Definieren der Funktionalitäten und Aufgaben der einzelnen Klassen

## [1.0.0] - 13.12.2021

### added by Angelina, Eva, Michael, Pamela, Volker

- Projektstruktur vereinbart
- Skizzen der Screens angefertigt
- Entwurf des grundsätzlichen Ablaufs und erste Überlegungen zu Funktionalitäten












