package level.editor.team.client.view.action;

import java.io.File;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

/**
 * GameMusic soll als Kanon im Abstand von einer Sekunde in einem neuen Thread gestartet werden. *
 * Methode PlayLevelEditorMusic.playMusicAsCanon() spielt die GameMusic im Kanon
 * (Spielhalleneffekt), funktioniert aber nicht ganz zuverlässig, deswegen Experimentalstatus und
 * nicht aus dem Projekt gelöscht
 */
public class GameMusicThreadExperiment extends Thread {

  @Override
  public void run() {
    String path2 =
        PlayLevelEditorMusic.class.getResource("/ArnheitersPixelRunnerMusic.wav").getPath();
    try {
      AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(path2));
      Clip clip = AudioSystem.getClip();
      clip.open(audioInputStream);
      clip.start();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
