package level.editor.team.client.view;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import javafx.animation.SequentialTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.Circle;
import javafx.util.Duration;
import level.editor.team.client.Client;
import level.editor.team.client.model.GameState;
import level.editor.team.client.model.JumpnRun;
import level.editor.team.client.model.board.Coordinate;
import level.editor.team.client.model.board.Figure;
import level.editor.team.client.model.board.Level;
import level.editor.team.client.view.action.PlayLevelEditorMusic;

/** Klasse steuert den GameScreen. Sie beinhaltet ein GridPane und baut die View des Levels auf. */
public class GameScreenControl {

  private final Level level;
  boolean success;
  boolean fail;
  boolean dangerousBlockContact;

  private final double accelerationUp;
  private final double accelerationDown;
  private final double deltaT = 0.4;

  double jumpVelocity;
  double runVelocity;
  // akutuelle x- und y-Koordinate des Players in Pixeln auf der Zeichenfläche
  @FXML Circle player;
  // aktuelle Spalten- und Zeilennummer des Players im GridPane
  int actuelColumn;
  double actuelX;
  double actuelY;
  int actuellRow;
  @FXML Arc finish;

  @FXML Arc start;
  @FXML GridPane gridPane;
  // Pixelkoordinaten der Ränder
  int leftBorder;
  private final Figure figure;
  // Pixelkoordinaten des Ziels
  private int finishColumn;
  private int finishRow;
  private int finishMinX;
  private int finishMaxX;
  private int finishMaxY;
  private int finishMinY;
  int rightBorder;
  int topBorder;
  int bottomBorder;

  private int blockSize;
  private List<Label> blocks;
  private List<Label> dangerousBlocks;
  private List<int[]> blockCoordinates;
  private List<int[]> dangerousBlockCoordinates;

  @FXML Label pixelRunnerLabel;
  @FXML Label spielerNameAnzeigenLabel;
  @FXML Label levelNameAnzeigenLabel;
  @FXML Label nummerAnzeigenLabel;
  @FXML Label bestzeitAnzeigenLabel;
  @FXML Label playTimeLabel;
  @FXML Label gameStatusLabel;
  @FXML Button backToMenuButton;
  @FXML Button replayButton;

  private final JumpnRun jumpnRun;
  private final Client client;

  private final boolean hasDangerousBlocks;

  // Volkers tolle Musik wir während des gesamten Spiels gespielt
  private final PlayLevelEditorMusic music;

  private static final DecimalFormat fmtDoubleDigits = new DecimalFormat("00");
  private static final DecimalFormat fmtTripleDigits = new DecimalFormat("000");

  /** Konstruktor des GameScreenConrollers. */
  public GameScreenControl(Client client, JumpnRun jumpnRun) {
    this.client = client;
    this.jumpnRun = jumpnRun;
    figure = client.getFigure();
    accelerationUp = -9.8;
    accelerationDown = -15;
    runVelocity = 0;
    jumpVelocity = 0;
    level = jumpnRun.getGameState().getLevel();

    success = false;
    fail = false;
    dangerousBlockContact = false;

    // beim ersten Versuch ein Level zu spielen, enthält es nie gefährliche Blöcke
    if (client
        .getClientCommunication()
        .getStatisticMap()
        .get(level.getLevelname())
        .get(2)
        .equals("0")) {
      hasDangerousBlocks = false;
    } else {
      Random rand = new Random();
      hasDangerousBlocks = rand.nextBoolean();
    }
    music = new PlayLevelEditorMusic();
  }

  // Blöcke, Start und Ende werden an die richtigen Stellen gesetzt (aus Level)
  // Figur (Eigenschaften des Players) wird initialisiert,
  // PlayerFigur wird rechts vom Startblock platziert, Anzeigen werden gesetzt

  // Es wird komplett im Pixeln im Koordinatensystem des Players (das immer an derselben Stelle im
  // GridPane liegen bleibt) gerechnet
  @FXML
  void initialize() {

    player.setFill(figure.getColor());

    blocks = new ArrayList<>();
    blockCoordinates = new ArrayList<>();
    dangerousBlocks = new ArrayList<>();
    dangerousBlockCoordinates = new ArrayList<>();

    blockSize = (int) player.getRadius();

    // GridPane wird initialisiert
    int gridSize = level.getGridSize();

    for (int i = 0; i < gridSize; i++) {
      for (int j = 0; j < gridSize; j++) {

        Label label = new Label("");
        label.setStyle("-fx-border-color: \"grey\"; -fx-border-width: 0.25px;");
        label.setMinWidth(blockSize);
        label.setMinHeight(blockSize);
        label.setAlignment(Pos.CENTER);
        label.setViewOrder(4);

        Coordinate coord = Coordinate.of(i, j);

        // Eingang und Ziel werden entsprechend des Levels auf dem GridPane gesetzt, der Player
        // startet immer direkt rechts vom Eingang. Die Spielfeldgrenzen werden berechnet.
        // Falls es sich um ein Spiel mit gefährlichen Blöcken handelt, werden diese für jeden Block
        // mit 10%-iger Wahrscheinlichkeit erzeugt und ggf. über dem normalen Block gesetzt
        // (maximale Höhe über dem Block: 4)
        if (level.isStart(coord)) {
          GridPane.setConstraints(start, j, i);
          GridPane.setConstraints(player, j + 1, i);
          actuelColumn = j + 1;
          actuellRow = i;

          // Setzen der Spielfeldgrenzen
          leftBorder = (-actuelColumn) * blockSize;
          rightBorder = (gridSize - actuelColumn) * blockSize;
          topBorder = (actuellRow + 1) * blockSize;
          bottomBorder = (actuellRow - gridSize + 1) * blockSize;
        }
        if (level.isFinish(coord)) {
          GridPane.setConstraints(finish, j, i);

          finishColumn = j;
          finishRow = i;
        } else if (level.isBlock(coord)) { // Blöcke werden eingefärbt
          // mit einer Wahrscheinlichkeit von 10% erhält jeder Block einen gefährlichen Block mit
          // dem Abstand 0, 1, 2 oder 3 über sich
          boolean isPossibleForDanger = true;

          // Start- und Zielblock sowie die Pixel hinter dem Player werden als Stellen für
          // gefährliche Blöcke ausgeschlossen
          Coordinate[] noCoordinatesForDanger = new Coordinate[10];

          if (j > 0) {
            noCoordinatesForDanger[0] = Coordinate.of(i, j - 1);
          }
          if (j < gridPane.getColumnCount() - 1) {
            noCoordinatesForDanger[1] = Coordinate.of(i, j + 1);
          }
          if (i > 0 && j > 0) {
            noCoordinatesForDanger[2] = Coordinate.of(i - 1, j - 1);
          }
          if (i > 0) {
            noCoordinatesForDanger[3] = Coordinate.of(i - 1, j);
          }
          if (i > 0 && j < gridPane.getColumnCount() - 1) {
            noCoordinatesForDanger[4] = Coordinate.of(i - 1, j + 1);
          }
          if (i > 1 && j > 0) {
            noCoordinatesForDanger[5] = Coordinate.of(i - 2, j - 1);
          }
          if (i > 1) {
            noCoordinatesForDanger[6] = Coordinate.of(i - 2, j);
          }
          if (i > 1 && j < gridPane.getColumnCount() - 1) {
            noCoordinatesForDanger[7] = Coordinate.of(i - 2, j + 1);
          }
          if (j > 1) {
            noCoordinatesForDanger[8] = Coordinate.of(i, j - 2);
          }
          if (i > 1 && j > 1) {
            noCoordinatesForDanger[9] = Coordinate.of(i - 1, j - 2);
          }

          for (int k = 0; k < noCoordinatesForDanger.length; k++) {
            if (noCoordinatesForDanger[k] != null) {
              if (level.isStart(noCoordinatesForDanger[k])
                  || level.isFinish(noCoordinatesForDanger[k])) {
                isPossibleForDanger = false;
              }
            }
          }

          if (hasDangerousBlocks && isPossibleForDanger) {
            Random dangerousBlockGenerator = new Random();
            int probability = dangerousBlockGenerator.nextInt(10);
            if (probability == 0) {
              Label dangerousLabel = new Label("");
              dangerousLabel.setStyle("-fx-border-color: \"grey\"; -fx-border-width: 0.2px;");
              dangerousLabel.setMinWidth(blockSize);
              dangerousLabel.setMinHeight(blockSize);
              dangerousLabel.setAlignment(Pos.CENTER);
              dangerousLabel.setViewOrder(3);
              dangerousLabel.setBackground(
                  new Background(
                      new BackgroundFill(Color.RED, new CornerRadii(0), new Insets(0, 0, 0, 0))));
              int dangerousBlockDistance = dangerousBlockGenerator.nextInt(5);
              // ohne Math.max gibt es zeitweise eine InvocationTargetException
              gridPane.add(dangerousLabel, j, Math.max(i - dangerousBlockDistance, 0));
              GridPane.setConstraints(dangerousLabel, j, Math.max(i - dangerousBlockDistance, 0));
              dangerousBlocks.add(dangerousLabel);
              blocks.add(dangerousLabel);
            }
          }

          label.setBackground(
              new Background(
                  new BackgroundFill(Color.CADETBLUE, new CornerRadii(0), new Insets(0, 0, 0, 0))));
          blocks.add(label);
        }
        gridPane.add(label, j, i);
        GridPane.setConstraints(label, j, i);
      }
    }

    // Für das Ziel wird die linke Grenze (minX), die rechte Grenze (maxX),
    // die untere Grenze (minY) und die obere Grenze (maxY) gespeichert
    finishMinX = (finishColumn - actuelColumn - 1) * blockSize;
    finishMaxX = (finishColumn - actuelColumn + 2) * blockSize;
    finishMinY = (actuellRow - finishRow) * blockSize;
    finishMaxY = (actuellRow - finishRow + 2) * blockSize;

    // zu jedem Block wird die linke Grenze (minX), die rechte Grenze (maxX),
    // die untere Grenze (minY) und die obere Grenze (maxY) in einem Array gespeichert. Alle Arrays
    // von Blockgrenzen werden in einer Liste verwaltet
    for (Label block : blocks) {
      int j = GridPane.getRowIndex(block);
      int i = GridPane.getColumnIndex(block);

      int minX = (i - actuelColumn) * blockSize;
      int maxX = (i - actuelColumn + 1) * blockSize;
      int minY = (actuellRow - j) * blockSize;
      int maxY = (actuellRow - j + 1) * blockSize;

      int[] minMax = {minX, maxX, minY, maxY};
      blockCoordinates.add(minMax);

      // falls ein Block gefährlich ist, werden seine Koordinaten in der Liste
      // dangerousBlockCoordinates gespeichert
      if (dangerousBlocks.contains(block)) {
        dangerousBlockCoordinates.add(minMax);
      }
    }

    // Sortieren der Reihenfolge der Darstellung
    pixelRunnerLabel.setVisible(false);
    gameStatusLabel.setVisible(false);
    gameStatusLabel.setViewOrder(0);
    player.setViewOrder(1);
    finish.setViewOrder(2);
    start.setViewOrder(2);

    spielerNameAnzeigenLabel.setText(client.getClientCommunication().getPlayerName());
    levelNameAnzeigenLabel.setText(level.getLevelname());

    nummerAnzeigenLabel.setText(
        String.valueOf(
            Integer.parseInt(
                    client
                        .getClientCommunication()
                        .getStatisticMap()
                        .get(level.getLevelname())
                        .get(2))
                + 1));
    if (client
        .getClientCommunication()
        .getStatisticMap()
        .get(level.getLevelname())
        .get(1)
        .equals("0")) {
      bestzeitAnzeigenLabel.setText("--");
    } else {
      bestzeitAnzeigenLabel.setText(
          Double.parseDouble(
                      client
                          .getClientCommunication()
                          .getStatisticMap()
                          .get(level.getLevelname())
                          .get(0))
                  / 1000
              + " s");
    }

    // die Uhr startet mit regelmäßer Aktualisierung der Anzeige
    Timer timer = new Timer();
    timer.scheduleAtFixedRate(wrap(() -> updatePlayTime()), 1000, 100);

    // Setzen der EventHandler
    backToMenuButton.addEventHandler(KeyEvent.KEY_PRESSED, this::move);
    backToMenuButton.addEventHandler(KeyEvent.KEY_RELEASED, this::move);
    backToMenuButton.addEventHandler(MouseEvent.MOUSE_CLICKED, this::goToMenu);
    replayButton.addEventHandler(MouseEvent.MOUSE_CLICKED, this::replay);

    // Initial befindet sich der Player bei (0|0) seines eigenen KoordinatenSystems
    actuelX = 0;
    actuelY = 0;

    music.loadAndPlayMusic();
  }

  // Methode für jede Art von tastaturgesteuerter Bewegung (laufen, springen)
  private void move(KeyEvent keyEvent) {

    if (!success && !fail) {
      if (keyEvent.getEventType().equals(KeyEvent.KEY_RELEASED)) {
        runVelocity = 0;
        jumpVelocity = 0;
      } else if (keyEvent.getCode() == KeyCode.RIGHT && runVelocity == 0) {
        runVelocity = figure.getRunVelocity();
        run();
      } else if (keyEvent.getCode() == KeyCode.LEFT && runVelocity == 0) {
        runVelocity = -figure.getRunVelocity();
        run();
      } else if (keyEvent.getCode().isWhitespaceKey() && jumpVelocity == 0) {
        jumpVelocity = figure.getJumpVelocity();
        jump();
      }
      keyEvent.consume();
    }
  }

  /**
   * Methode zum Laufen, bei horizontalem Anstoßen endet die Bewegung in x-Richtung pro
   * Tastatur-Klick ein Schritt in x-Richtung.
   */
  public void run() {
    // mögliches Zusatzfeature für beliebige Laufgeschwindigkeiten: actuellX in [minX, maxX] eines
    // Blocks Todo erledigt

    // Wenn der Player an einen Block anstößt, wird die Laufgeschwindigkeit auf null gesetzt. Stößt
    // er dabei an einen gefährlichen Block, so verliert er das Spiel.
    for (int[] blockCoordinate : blockCoordinates) {
      if ((actuelY == blockCoordinate[2] || actuelY + player.getRadius() == blockCoordinate[2])
          && ((actuelX + 2 * player.getRadius() + runVelocity >= blockCoordinate[0]
                  && actuelX + 2 * player.getRadius() + runVelocity <= blockCoordinate[1]
                  && runVelocity > 0)
              || (actuelX + runVelocity <= blockCoordinate[1]
                  && actuelX + runVelocity >= blockCoordinate[0]
                  && runVelocity < 0))) {
        if (runVelocity > 0) {
          runVelocity = blockCoordinate[0] - (actuelX + 2 * player.getRadius());
        } else {
          runVelocity = blockCoordinate[1] - actuelX;
        }
      } else if ((actuelY == blockCoordinate[2]
              || actuelY + player.getRadius() == blockCoordinate[2])
          && ((actuelX + 2 * player.getRadius() == blockCoordinate[0] && runVelocity > 0)
              || (actuelX == blockCoordinate[1] && runVelocity < 0))) {
        runVelocity = 0;
      }
    }

    TranslateTransition transition = new TranslateTransition();
    transition.setDuration(Duration.millis(10));

    // Wenn der Player nicht an einem seitlichen Rand anstößt, bewegt er sich in Laufrichtung. Läuft
    // er dabei auf einen gefährlichen Block, so verliert er das Spiel.
    if ((actuelX > leftBorder && actuelX < rightBorder - 2 * player.getRadius()
        || (actuelX == leftBorder && runVelocity > 0)
        || (actuelX == rightBorder - 2 * player.getRadius() && runVelocity < 0))) {
      transition.setByX(runVelocity);
      actuelX = actuelX + runVelocity;
      actuelColumn = (int) (actuelColumn + Math.signum(runVelocity) * 1);
    }
    transition.setNode(player);
    transition.play();

    checkForDangerousBlocksContact(actuelX, actuelY);
    checkSuccess();

    // nach jedem Schritt kann der Player fallen
    fall();
  }

  /** Methode zum Springen. */
  public void jump() {
    SequentialTransition jumpUpAndDown = new SequentialTransition();
    int deltaYgesamt = 0;
    if (runVelocity != 0 && jumpVelocity > 0) {

      boolean moveToRight = runVelocity > 0;

      // solange die Geschwindigkeit in y-Richtung positiv ist: Transitions gemäß der Methode der
      // kleinen Schritte
      while (jumpVelocity > 0) {
        TranslateTransition transition = new TranslateTransition();
        transition.setDuration(Duration.millis(deltaT * 100));

        int deltaY = (int) (jumpVelocity * deltaT + 0.5 * accelerationUp * deltaT * deltaT);
        deltaYgesamt += deltaY;
        jumpVelocity = jumpVelocity + accelerationUp * deltaT;

        // wird die Geschwindigkeit in y-Richtung negativ, so wird sie auf null gesetzt
        if (jumpVelocity <= 0) {
          jumpVelocity = 0;
          // Falls Sprung nicht auf Höhe eines Blocks endet
          if (deltaYgesamt % blockSize > 0) {
            // Korrektur der Höhe des Players auf Höhe eines Blockes
            transition.setByY(-(deltaY - deltaYgesamt % blockSize));
            actuelY = actuelY + (deltaY - deltaYgesamt % blockSize);
          }
        } else {
          transition.setByY(-deltaY);
          actuelY = actuelY + deltaY;
        }

        transition.setByX(deltaT * runVelocity);
        transition.setNode(player);
        jumpUpAndDown.getChildren().add(transition);

        actuelX = actuelX + deltaT * runVelocity;
      }

      // prüfe, ob der player auf einem gefährlichen Block landet
      checkForDangerousBlocksContact(actuelX, actuelY);

      // sobald der Player den Parabelscheitel erreicht hat, fällt er wieder
      double fallVelocity = 0;
      boolean isOnAblock = false;

      // prüfen, ob es einen Block gibt, auf dem der Player sitzt
      for (int[] blockCoordinate : blockCoordinates) {
        if (actuelY == blockCoordinate[3]
            && (actuelX + player.getRadius() >= blockCoordinate[0]
                && actuelX + player.getRadius() <= blockCoordinate[1])) {
          isOnAblock = true;
        }
      }

      // falls der Player nicht auf einem Block sitzt, muss er fallen
      if (!isOnAblock) {
        int stoppingBlockMaxY;
        double stoppingBlockMinX = 0;
        double stoppingBlockMaxX = 0;

        // solange der Player den unteren Rand nicht berührt, soll er fallen
        while (actuelY > bottomBorder && !isOnAblock) {
          jumpVelocity = 0;
          TranslateTransition fallTransition = new TranslateTransition();
          fallTransition.setDuration(Duration.millis(deltaT * 100));
          // Fallhöhe jeder Transition wird berechnet (negativ)
          int deltaY = (int) (fallVelocity * deltaT + 0.5 * accelerationDown * deltaT * deltaT);
          // Fallgeschwindigkeit nach wird nach jeder Transition angepasst
          fallVelocity = fallVelocity + accelerationDown * deltaT;
          stoppingBlockMaxY = (int) actuelY + deltaY;
          // Bewegung in x-Richtung bei jeder Transition um deltaX
          int deltaX = (int) (0.5 * runVelocity);
          if (moveToRight) {
            stoppingBlockMinX = actuelX + deltaX;
            stoppingBlockMaxX = stoppingBlockMinX + blockSize;
          } else {
            stoppingBlockMaxX = actuelX + deltaX;
            stoppingBlockMinX = stoppingBlockMaxX - blockSize;
          }

          // prüfen, ob ein Block im Weg ist
          for (int[] blockCoordinate : blockCoordinates) {
            // wenn Block im Weg ist, setze Player auf den Block
            if (actuelY > blockCoordinate[3] && actuelY + deltaY <= blockCoordinate[3]) {

              if ((moveToRight
                      && (actuelX + 2 * player.getRadius() <= blockCoordinate[0]
                              && actuelX + 2 * player.getRadius() + deltaX >= blockCoordinate[0]
                          || (actuelX + 2 * player.getRadius() <= blockCoordinate[1]
                              && actuelX + 2 * player.getRadius() + deltaX >= blockCoordinate[1])
                          || (actuelX + 2 * player.getRadius() >= blockCoordinate[0]
                              && actuelX + 2 * player.getRadius() <= blockCoordinate[1]
                              && actuelX + 2 * player.getRadius() + deltaX >= blockCoordinate[0]
                              && actuelX + 2 * player.getRadius() + deltaX <= blockCoordinate[1]))
                  || (!moveToRight
                          && (actuelX >= blockCoordinate[1]
                              && actuelX + deltaX <= blockCoordinate[1])
                      || (actuelX >= blockCoordinate[0] && actuelX + deltaX <= blockCoordinate[0])
                      || (actuelX >= blockCoordinate[0]
                          && actuelX + deltaX >= blockCoordinate[0]
                          && actuelX <= blockCoordinate[1]
                          && actuelX + deltaX <= blockCoordinate[1])))) {
                // falls Block höher liegt als der letzte mit diesen Eigenschaften: setze Oberkante
                // höher
                if (blockCoordinate[3] > stoppingBlockMaxY) {
                  stoppingBlockMaxY = blockCoordinate[3];
                  stoppingBlockMinX = blockCoordinate[0];
                  stoppingBlockMaxX = blockCoordinate[1];
                }
                isOnAblock = true;
              }
            }
          }

          fallTransition.setByY(actuelY - stoppingBlockMaxY);
          // berechne die aktuellen Koordinaten des Players in seinem eigenen Koordinatensystem
          actuelY = stoppingBlockMaxY;
          fallTransition.setByX(deltaX);
          actuelX = actuelX + deltaX;

          fallTransition.setNode(player);
          jumpUpAndDown.getChildren().add(fallTransition);
          checkForDangerousBlocksContact(actuelX, actuelY);
          if (dangerousBlockContact) {
            break;
          }
        }
        // Korrektur in x-Richtung (nicht mehr nötig)
        TranslateTransition correctionTransition = new TranslateTransition();
        correctionTransition.setDuration(Duration.millis(deltaT));
        correctionTransition.setNode(player);
      }
      jumpUpAndDown.play();
      checkSuccess();

    } else {
      // falls der Player keine Geschwindigkeit in x-Richtung hat soll er nur nach oben springen
      // und an Blöcken von unten anstoßen und abprallen
      TranslateTransition transition = new TranslateTransition();
      transition.setDuration(Duration.millis(200));

      // Anstoßen an einen Block von unten
      double deltaY = 2 * jumpVelocity;
      for (int[] blockCoordinate : blockCoordinates) {
        if ((actuelX + player.getRadius() >= blockCoordinate[0]
                && actuelX + player.getRadius() <= blockCoordinate[1])
            && (actuelY + 2 * player.getRadius() <= blockCoordinate[2])) {
          // Minimum der Unterkanten über dem Player finden
          if (blockCoordinate[2] - 2 * player.getRadius() <= deltaY) {
            deltaY = blockCoordinate[2] - actuelY - 2 * player.getRadius();

            // Falls ein gefährlicher Block berührt wurde
            if (dangerousBlockCoordinates.contains(blockCoordinate)) {
              dangerousBlockContact = true;
            }
          }
        }
      }
      if (jumpVelocity > 0) {
        transition.setAutoReverse(true);
        transition.setCycleCount(2);
        transition.setByY(-1 * deltaY);
        transition.setNode(player);
        transition.play();

        checkFail();
      }
    }
  }

  /** Methode zum Fallen. */
  private synchronized void fall() {

    SequentialTransition fallDown = new SequentialTransition();

    boolean isOnAblock = false;
    double fallVelocity = 0;
    int stoppingBlockMaxY;
    double stoppingBlockMinX;
    double stoppingBlockMaxX;

    for (int[] blockCoordinate : blockCoordinates) {
      if (actuelY == blockCoordinate[3]
          && (actuelX + player.getRadius() >= blockCoordinate[0]
              && actuelX + player.getRadius() <= blockCoordinate[1])) {
        isOnAblock = true;
      }
    }

    // solange der Player den unteren Rand nicht berührt, soll er fallen
    while (actuelY > bottomBorder && !isOnAblock) {
      TranslateTransition fallTransition = new TranslateTransition();
      fallTransition.setDuration(Duration.millis(deltaT * 100));
      // Fallhöhe jeder Transition wird berechnet (negativ)
      int deltaY = (int) (fallVelocity * deltaT + 0.5 * accelerationDown * deltaT * deltaT);
      // Fallgeschwindigkeit nach wird nach jeder Transition angepasst
      fallVelocity = fallVelocity + accelerationDown * deltaT;
      stoppingBlockMaxY = (int) actuelY + deltaY;
      // Bewegung in x-Richtung bei jeder Transition um deltaX
      int deltaX = (int) (0.5 * runVelocity);
      if (runVelocity > 0) {
        stoppingBlockMinX = actuelX + deltaX;
        stoppingBlockMaxX = stoppingBlockMinX + blockSize;
      } else {
        stoppingBlockMaxX = actuelX + deltaX;
        stoppingBlockMinX = stoppingBlockMaxX - blockSize;
      }

      // prüfen, ob ein Block im Weg ist
      for (int[] blockCoordinate : blockCoordinates) {
        // wenn Block im Weg ist, setze Player auf den Block
        if (actuelY > blockCoordinate[3]
            && actuelY + deltaY <= blockCoordinate[3]) { // vor dem Fall höher, nach dem Fall tiefer

          // je nachdem, ob sich der Player nach rechts oder links bewegt, trifft er von
          // verschiedenen Seiten auf die Blöcke
          if ((runVelocity > 0
                  && (actuelX + 2 * player.getRadius() <= blockCoordinate[0]
                          && actuelX + 2 * player.getRadius() + deltaX >= blockCoordinate[0]
                      || (actuelX + 2 * player.getRadius() <= blockCoordinate[1]
                          && actuelX + 2 * player.getRadius() + deltaX >= blockCoordinate[1])
                      || (actuelX + 2 * player.getRadius() >= blockCoordinate[0]
                          && actuelX + 2 * player.getRadius() <= blockCoordinate[1]
                          && actuelX + 2 * player.getRadius() + deltaX >= blockCoordinate[0]
                          && actuelX + 2 * player.getRadius() + deltaX <= blockCoordinate[1]))
              || (runVelocity < 0
                      && (actuelX >= blockCoordinate[1] && actuelX + deltaX <= blockCoordinate[1])
                  || (actuelX >= blockCoordinate[0] && actuelX + deltaX <= blockCoordinate[0])
                  || (actuelX >= blockCoordinate[0]
                      && actuelX + deltaX >= blockCoordinate[0]
                      && actuelX <= blockCoordinate[1]
                      && actuelX + deltaX <= blockCoordinate[1])))) {
            // falls Block höher liegt als der letzte mit diesen Eigenschaften: setze Oberkante
            // höher
            if (blockCoordinate[3] > stoppingBlockMaxY) {
              stoppingBlockMaxY = blockCoordinate[3];

              stoppingBlockMinX = blockCoordinate[0];
              stoppingBlockMaxX = blockCoordinate[1];
            }
            isOnAblock = true;
          }
        }
      }

      fallTransition.setByY(actuelY - stoppingBlockMaxY);
      // berechne die aktuellen Koordinaten des Players in seinem eigenen Koordinatensystem
      actuelY = stoppingBlockMaxY;
      fallTransition.setByX(deltaX);
      actuelX = actuelX + deltaX;

      fallTransition.setNode(player);
      fallDown.getChildren().add(fallTransition);
      checkForDangerousBlocksContact(actuelX, actuelY);
      if (dangerousBlockContact) {
        break;
      }
    }
    // Korrektur in x-Richtung (nicht mehr nötig)
    TranslateTransition correctionTransition = new TranslateTransition();
    correctionTransition.setDuration(Duration.millis(deltaT));
    correctionTransition.setNode(player);

    fallDown.play();
  }

  /**
   * Methode, die prüft, ob der Player gefährliche Blöcke berührt.
   *
   * @param actuellX aktuelle x-Position des Players in seinem Koordinatensystem
   * @param actuellY aktuelle y-Position des Players in seinem Koordinatensystem
   */
  private void checkForDangerousBlocksContact(double actuellX, double actuellY) {
    for (int[] dangerousBlockCoordinate : dangerousBlockCoordinates) {
      if ((actuellX + 2 * player.getRadius() >= dangerousBlockCoordinate[0]
              && actuellX <= dangerousBlockCoordinate[1]
              && actuellY + player.getRadius() >= dangerousBlockCoordinate[2]
              && actuellY + player.getRadius() <= dangerousBlockCoordinate[3])
          || (actuellX + player.getRadius() >= dangerousBlockCoordinate[0]
              && actuellX + player.getRadius() <= dangerousBlockCoordinate[1]
              && actuellY <= dangerousBlockCoordinate[3]
              && actuellY >= dangerousBlockCoordinate[2])) {
        dangerousBlockContact = true;
      }
    }
    checkFail();
  }

  /** Methode, die prüft, ob das Spiel gewonnen ist und das #JumpnRun darüber informiert. */
  @FXML
  private synchronized void checkSuccess() {
    if (actuelX + player.getRadius() >= finishMinX
        && actuelX + player.getRadius() <= finishMaxX
        && actuelY >= finishMinY
        && actuelY <= finishMaxY) {
      success = true;
      jumpnRun.setGameIsWon();
      sendLevelPlayedMessage();
      gameStatusLabel.setVisible(true);
    }
  }

  /** Methode, die prüft, ob das Spiel verloren ist und das #JumpnRun darüber informiert. */
  private synchronized void checkFail() {
    if (actuelY <= bottomBorder || dangerousBlockContact) {
      fail = true;
      jumpnRun.setGameIsLost();
      sendLevelPlayedMessage();
      gameStatusLabel.setText(client.getActiveBundle().getString("GameScreen.lost"));
      gameStatusLabel.setVisible(true);
    }
  }

  /**
   * Methode, die eine Message zum Server schickt, dass das Level gespielt wurde. Die Message
   * enthält die StatistikInformationen bzgl. des Spiels.
   */
  private synchronized void sendLevelPlayedMessage() {
    client
        .getClientCommunication()
        .levelPlayed(
            client.getClientCommunication().getPlayerName(),
            level.getLevelname(),
            jumpnRun.getPlayTimeOfGame(),
            jumpnRun.getGameState().isGameWon());
  }

  /**
   * Methode, um vom GameScrren zum MenuScreen zurückzukehren.
   *
   * @param mouseEvent Wenn der backToMenuButton angeklickt wird, wird die Methode aufgerufen.
   */
  private void goToMenu(MouseEvent mouseEvent) {
    if (jumpnRun.getGameState().isGameRunning()) {
      jumpnRun.setGameIsLost();
    }
    music.stopMusic();
    client.showMenueScreen();
    mouseEvent.consume();
  }

  /**
   * Methode um das gleiche Leven nochmal zu spielen, ohne auf den MenüBildschirm zurück zu müssen.
   *
   * @param mouseEvent Wenn der replayButton angeklickt wird, wird die Methode aufgerufen.
   */
  private void replay(MouseEvent mouseEvent) {
    music.stopMusic();
    client.playGame(level.getLevelname());
  }

  private static TimerTask wrap(Runnable r) {
    return new TimerTask() {

      @Override
      public void run() {
        r.run();
      }
    };
  }

  // in einem Timer jede Sekunde aufgerufen, nachdem das Spiel gestartet wurde.
  private void updatePlayTime() {
    // so lange läuft das Spiel schon
    long millis;
    if (jumpnRun.getStatus() == GameState.GameStatus.ONGOING) {
      millis = jumpnRun.getPlayActualTime();
    } else {
      millis = jumpnRun.getPlayTimeOfGame();
    }

    long millisecs = (millis % 1000);
    long allsecs = millis / 1000;
    long secs = allsecs % 60;
    long allminutes = allsecs / 60;
    long minutes = allminutes % 60;

    // synchronisierte Anzeige auf dem Game Screen
    Platform.runLater(
        () ->
            playTimeLabel.setText(
                client.getActiveBundle().getString("GameScreen.duration")
                    + fmtDoubleDigits.format(minutes)
                    + ":"
                    + fmtDoubleDigits.format(secs)
                    + "."
                    + fmtTripleDigits.format(millisecs)));
  }
}
