package level.editor.team.client.model.board;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import level.editor.team.communication.messages.Message;
import level.editor.team.communication.messages.TransferLevel;

/**
 * Klasse repräsentiert ein Level des Spiels, einschließlich der Blöcke, Eingang und Ausgang. Das
 * Level besteht aus einem zweidimensionalen Array als Pixelfeld der Größe @gridSize x @gridSize aus
 * Zeilen- und Spaltennummern. Eine TreeMap enthält die Position der Blöcke, des Ein- und des
 * Ausgangs. Es gibt keine Methode, mit der man abfragen kann, wo sich die Spielfigur zu Beginn
 * befindet, weil sie immer eine Zelle rechts vom Start ist.
 */
public class Level {

  // Liste aller Koordinaten
  List<Coordinate> coordinates = new ArrayList<>();
  // Map mit allen Koordinaten der Pixel, die einen Block, den Ein- oder der Ausgang enthalten
  TreeMap<Coordinate, Pixel> pixels = new TreeMap<>();
  private final String levelname;
  private final int gridSize = 50;

  /**
   * Erzeugt ein Level aus der Message.
   *
   * @param message Die Message
   */
  public Level(Message message) {

    final List<String> list = ((TransferLevel) message).getLevel();
    this.levelname = ((TransferLevel) message).getLevelName();

    createAllCoordinates();
    // Message zeilenweise auslesen
    String pixelString;
    Coordinate coordinate;
    Pixel pixel;
    int row;
    int column;
    boolean isStart;
    boolean isFinish;
    boolean isBlock;
    String[] pixelWords;

    for (String s : list) {
      pixelString = s;

      // Eintrag nach Leerzeichen zerlegen und TreeMap füllen
      pixelWords = pixelString.split(" ");

      row = Integer.parseInt(pixelWords[0]);
      column = Integer.parseInt(pixelWords[1]);
      isStart = Boolean.parseBoolean(pixelWords[2]);
      isFinish = Boolean.parseBoolean(pixelWords[3]);
      isBlock = Boolean.parseBoolean(pixelWords[4]);

      coordinate = Coordinate.of(row, column);
      pixel = new Pixel(isBlock, isStart, isFinish);
      pixels.put(coordinate, pixel);
    }
  }

  /** Konstruktor erzeugt ein Sprunglevel zu Testzwecken. */
  public Level() {
    levelname = "SprungLevel";
    createAllCoordinates();

    for (Coordinate coordinate : coordinates) {
      if (coordinate.getColumnNumber() > 2
          && coordinate.getColumnNumber() < 15
          && coordinate.getRowNumber() == 40) {
        Pixel pixel = new Pixel(true, false, false);
        pixels.put(coordinate, pixel);
      } else if (coordinate.getColumnNumber() > 12
          && coordinate.getColumnNumber() < 25
          && coordinate.getRowNumber() == 35) {
        Pixel pixel = new Pixel(true, false, false);
        pixels.put(coordinate, pixel);
      } else if (coordinate.getColumnNumber() > 26
          && coordinate.getColumnNumber() < 32
          && coordinate.getRowNumber() == 31) {
        Pixel pixel = new Pixel(true, false, false);
        pixels.put(coordinate, pixel);
      } else if (coordinate.getColumnNumber() > 33
          && coordinate.getColumnNumber() < 38
          && coordinate.getRowNumber() == 27) {
        Pixel pixel = new Pixel(true, false, false);
        pixels.put(coordinate, pixel);
      } else if (coordinate.getColumnNumber() > 39
          && coordinate.getColumnNumber() < 50
          && coordinate.getRowNumber() == 25) {
        Pixel pixel = new Pixel(true, false, false);
        pixels.put(coordinate, pixel);
      } else if (coordinate.getColumnNumber() == 7 && coordinate.getRowNumber() == 39) {
        Pixel pixel = new Pixel(false, true, false);
        pixels.put(coordinate, pixel);
      } else if (coordinate.getColumnNumber() == 47 && coordinate.getRowNumber() == 24) {
        Pixel pixel = new Pixel(false, false, true);
        pixels.put(coordinate, pixel);
      } else if (coordinate.getColumnNumber() > 32
          && coordinate.getColumnNumber() < 45
          && coordinate.getRowNumber() == 38) {
        Pixel pixel = new Pixel(true, false, false);
        pixels.put(coordinate, pixel);
      }
    }
  }


  /** Erzeugt alle Koordinaten eines Levels. */
  public void createAllCoordinates() {
    for (int i = 0; i < gridSize; i++) {
      for (int j = 0; j < gridSize; j++) {
        Coordinate coordinate = Coordinate.of(j, i);
        coordinates.add(coordinate);
      }
    }
  }


  public List<Coordinate> getAllCoordinates() {
    return coordinates;
  }

  /**
   * Gibt alle Koordinaten mit den zugehörigen Pixeln, die Blöcke, Ein- oder Ausgang enthalten.
   *
   * @return Die TreeMap
   */
  public TreeMap<Coordinate, Pixel> getPixels() {
    return pixels;
  }

  public boolean isBlock(Coordinate coordinate) {
    return (pixels.containsKey(coordinate) && pixels.get(coordinate).isBlock());
  }

  public boolean isFinish(Coordinate coordinate) {
    return (pixels.containsKey(coordinate) && pixels.get(coordinate).isFinish());
  }

  public boolean isStart(Coordinate coordinate) {
    return (pixels.containsKey(coordinate) && pixels.get(coordinate).isStart());
  }

  public Pixel getPixel(Coordinate coordinate) {
    return pixels.get(coordinate);
  }

  public int getGridSize() {
    return gridSize;
  }

  public String getLevelname() {
    return levelname;
  }
}
