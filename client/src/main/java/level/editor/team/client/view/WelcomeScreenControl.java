package level.editor.team.client.view;

import java.util.Locale;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import level.editor.team.client.Client;

/** Klasse steuert den WelcomeScreen. */
public class WelcomeScreenControl {

  //  public Button startButton;
  @FXML TextField nameLabel;

  // private Stage primaryStage;
  private Client client;

  // hier werden die auswählbaren Checkboxen fuer die Sprachauswahl angelegt
  @FXML private CheckBox cbGerman;

  @FXML private CheckBox cbEnglish;

  @FXML private CheckBox cbSpanish;

  @FXML private CheckBox cbFrench;

  public void setClient(Client client) {
    this.client = client;
  }

  /** Falls ein Speilername eingegben wurde wir der MenuScreen aufgerufen. */
  @FXML
  public void startClicked(ActionEvent event) {
    event.consume();
    // pruefe ob ein Name eingegeben wurde (exklusive Leerzeichen)
    // javafx messageBox show dialogue ->
    // https://stackoverflow.com/questions/11662857/javafx-2-1-messagebox
    String defaultText = client.getActiveBundle().getString("WelcomeScreen.Spielername");
    if (nameLabel.getText().equals(defaultText) || nameLabel.getText().trim().length() == 0) {
      Alert alert = new Alert(Alert.AlertType.INFORMATION);
      alert.setTitle(client.getActiveBundle().getString("WelcomeScreen.keinSpielername"));
      alert.setContentText(client.getActiveBundle().getString("WelcomeScreen.BitteNamenEingeben"));
      alert
          .showAndWait()
          .ifPresent(
              rs -> {
                if (rs == ButtonType.OK) {
                  System.out.println("Zum Bestätigen OK klicken.");
                }
              });
      // der Cursor springt direkt in das Namensfeld hinein
      nameLabel.requestFocus();
    } else {

      client.startCommunication(nameLabel.getText());
      client.showMenueScreen();
    }

    // FIXME !
    // MenuScreenControl menue = new MenuScreenControl();
    // menue.showMe(primaryStage);

  }

  /**
   * EventHandler für die Sprachauswahl werden gesetzt.
   *
   * @param event MouseClickedEvent
   */
  @FXML
  public void onLanguageSelected(ActionEvent event) {
    Locale locale = null;
    if (event.getSource().equals(cbGerman)) {
      locale = new Locale("de");
    } else if (event.getSource().equals(cbEnglish)) {
      locale = new Locale("en");
    } else if (event.getSource().equals(cbSpanish)) {
      locale = new Locale("es");
    } else if (event.getSource().equals(cbFrench)) {
      locale = new Locale("fr");
    }

    if (locale != null) {
      // Sprache wechseln
      client.changeDisplayLanguage(locale);
      client.showWelcomeScreen();
      event.consume();
    }
  }
}
