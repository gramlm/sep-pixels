package level.editor.team.client;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;
import level.editor.team.client.model.board.Level;
import level.editor.team.communication.Connection;
import level.editor.team.communication.messages.GameNotification;
import level.editor.team.communication.messages.JoinGameRequest;
import level.editor.team.communication.messages.LevelPlayedMessage;
import level.editor.team.communication.messages.LevelRequest;
import level.editor.team.communication.messages.MenuStatisticInformationMessage;
import level.editor.team.communication.messages.Message;
import level.editor.team.communication.messages.TransferLevel;

/**
 * Die Klasse ist zuständig für die Kommunikation mit dem Server, also Senden und Empfangen von
 * Messages und deren Verarbeitung. In Anlehnung an die Klasse Client#NetworkGame von Task
 * 3-solution. https://uni2work.ifi.lmu.de/course/W21/IfI/PEEGS-LNQ/file
 */
public class ClientCommunication {

  private static final int DEFAULT_PORT = 4441;
  private static final String DEFAULT_ADDRESS = "localhost";

  private final String username;
  private boolean isConnected = false;
  private Connection connection;
  private Level aktuellesLevel;
  private List<String> levelList;
  private Map<String, List<String>> statisticMap;
  private Socket socket;

  /**
   * Konstruktor der ClientCommunication. Der Username wird vom WelcomeScreen übergeben. Übernommen
   * aus der Lösung von Task 3-solution. https://uni2work.ifi.lmu.de/course/W21/IfI/PEEGS-LNQ/file
   *
   * @param username Name des Users. Kommt vom WelcomeScreen.
   */
  public ClientCommunication(String username) {
    this.username = username;
    try {
      start();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static void printErrorMessage(String str) {
    System.out.println("Error! " + str);
  }

  /**
   * Start-Methode der ClientCommunication. Übernommen aus der Lösung von Task 3-solution.
   * https://uni2work.ifi.lmu.de/course/W21/IfI/PEEGS-LNQ/file
   *
   * @throws IOException wirft IOExcepton falls die Verbindung nicht passt.
   */
  void start() throws IOException {

    String serverAddress = DEFAULT_ADDRESS;
    int port = DEFAULT_PORT;

    InetAddress inetAddress = null;
    try {
      inetAddress = InetAddress.getByName(serverAddress);
    } catch (UnknownHostException e) {
      printErrorMessage("Invalid server address: " + serverAddress);
      return;
    }
    assert inetAddress != null;

    // start a client and connect to server
    InetSocketAddress address = new InetSocketAddress(inetAddress, port);
    try {
      socket = new Socket(address.getAddress(), address.getPort());
    } catch (IOException e) {
      System.out.println("Connection lost. Shutting down: " + e.getMessage());
    }
    try {
      connection = new Connection(socket.getOutputStream(), socket.getInputStream());
    } catch (IOException e) {
      System.out.println("Connection nicht aufgebaut.");
      e.printStackTrace();
    }

    // senden einer JoinGameRequest
    joinGame(connection);
  }

  /**
   * Behandelt die Messages, welche der Client vom Server erhält. Übernommen aus der Lösung von Task
   * 3-solution. https://uni2work.ifi.lmu.de/course/W21/IfI/PEEGS-LNQ/file
   *
   * @param connection Die Verbindung zum Client
   * @throws IOException Falls Verbindungsfehler auftreten
   */
  public void handleMessage(Connection connection) throws IOException {

    while (true) {
      Message message = connection.read();
      if (message instanceof TransferLevel) {
        takeLevel(message);
      } else if (message instanceof GameNotification) {
        handleGameNotification(message);
      } else if (message instanceof MenuStatisticInformationMessage) {
        statisticInformations(message);
      } else {
        System.out.println("Unbekannte Nachricht erhalten");
      }
    }
  }

  /**
   * Verarbeitet die StatisticNachricht vom Server.
   *
   * @param message Nachricht mit den Statistikinformationen vom Server.
   */
  private void statisticInformations(Message message) {
    statisticMap = ((MenuStatisticInformationMessage) message).getMap();
  }

  /**
   * Nimmt die GameNotification entgegen und setzt den boolean isConnected auf true.
   *
   * @param message GameNotification vom Server.
   */
  private void handleGameNotification(Message message) {
    // Nach dem Verbinden wird der boolean isConnected auf true gesetzt.
    isConnected = ((GameNotification) message).isConnected();

    // Die List der aktuellen Level kann an den Menuebildschirm übergeben werden.
    levelList = ((GameNotification) message).getLevelNamen();
  }

  /**
   * Das angefragte Level vom Server wird angenommen und übergeben.
   *
   * @param message Erhaltene Nachricht vom Server.
   */
  private void takeLevel(Message message) {
    // Schreibt das aktuelle Level mithilfe des Konstruktors in Level.

    aktuellesLevel = new Level(message);
  }

  public Level getAktuellesLevel() {
    return aktuellesLevel;
  }

  public String getPlayerName() {
    return this.username;
  }

  /**
   * Sendet die Registrationsmail an den Server. Übernommen aus der Lösung von Task 3-solution.
   * https://uni2work.ifi.lmu.de/course/W21/IfI/PEEGS-LNQ/file
   *
   * @param connection Die aktuelle connection.
   */
  private void joinGame(Connection connection) {
    try {
      connection.write(new JoinGameRequest(username));
    } catch (IOException e) {
      System.out.println("Senden der Registrationmail nicht möglich.");
      e.printStackTrace();
    }
  }

  /**
   * Sendet das erstellte Level.
   *
   * @param transferCreatedLevel Level-Message, die übertragen werden soll
   */
  public void createdNewLevel(Message transferCreatedLevel) {
    // Hier muss das Level in eine List<String> umgewandelt werden.
    try {
      connection.write(transferCreatedLevel);
    } catch (IOException e) {
      System.err.println("Neues Level konnte nicht übertragen werden");
    }
  }

  public List<String> getAvailableLevelNames() {
    return this.levelList;
  }

  public Map<String, List<String>> getStatisticMap() {
    return statisticMap;
  }

  /**
   * Fragt beim Server nach einem Level.
   *
   * @param levelName Der Name des Levels als String.
   */
  public void askForLevel(String levelName) {
    if (levelName == null || this.levelList == null || this.levelList.size() == 0) {
      return;
    }
    int levelNumber = 1;
    for (int i = 0; i < this.levelList.size(); i++) {
      if (levelName.equals(levelList.get(i))) {
        levelNumber = i + 1;
        break;
      }
    }
    // fire and forget
    LevelRequest message = new LevelRequest(username, levelNumber);
    try {
      connection.write(message);
    } catch (IOException e) {
      System.out.println("Senden um das Level zu bekommen nicht möglich.");
      e.printStackTrace();
    }
  }

  public Connection getConnection() {
    return connection;
  }

  /**
   * Sendet die LevelPlayedMessage, nachdem ein Level beendet wurde.
   *
   * @param playerName die Name des Players.
   * @param levelName der Name des gespielten Levels.
   * @param time die Zeit bis zum Beenden des Spiels.
   * @param success ob das Spiel erfolgreich beendet wurde.
   */
  public void levelPlayed(String playerName, String levelName, double time, boolean success) {
    LevelPlayedMessage levelPlayedMessage =
        new LevelPlayedMessage(playerName, levelName, time, success);
    try {
      connection.write(levelPlayedMessage);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
