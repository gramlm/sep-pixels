package level.editor.team.client.model;

import level.editor.team.client.model.GameState.GameStatus;
import level.editor.team.client.model.board.Figure;
import level.editor.team.client.model.board.Level;
import level.editor.team.client.view.PlayTime;

/**
 * Klasse ist zuständig für die Spiellogik: Verwaltet den Gamestate, weiß die Größe des Spielfeldes
 * und die Position der Spielfigur, enthält den OperationsStatus (Success, Fail), checkt, falls das
 * Spiel gewonnen bzw. verloren ist.
 *
 * <p>Diese Klasse wurde nach dem Vorbild der Lösungen von Task-3 (HighLowCardGame) und Task-4
 * (Bauernschach) erstellt. https://uni2work.ifi.lmu.de/course/W21/IfI/PEEGS-LNQ/file
 */
public class JumpnRun {

  private GameState gameState;
  private PlayTime playTime;
  private Figure figure;

  /**
   * Der Konstruktor des Spieles. Dabei wird der GameState und die Figur festgelegt. Zudem wird die
   * Spielzeit gestartet.
   *
   * @param playerName der Name des Spielers als String.
   * @param level Das zu spielende Level.
   * @param figure die ausgewählte Spielfigur.
   */
  public JumpnRun(String playerName, Level level, Figure figure) {
    gameState = new GameState(playerName, level);
    this.figure = figure;
    playTime = new PlayTime();
    playTime.start();
  }

  public GameState getGameState() {
    return gameState;
  }

  /** Setzt das Spiel auf verloren und stoppt die Zeit. */
  public void setGameIsLost() {
    gameState.setGameIsOver();
    playTime.stop();
  }

  /** Setzt das Spiel auf gewonnen und stoppt die Zeit. */
  public void setGameIsWon() {
    gameState.setGameIsWon();
    playTime.stop();
  }

  public GameStatus getStatus() {
    return gameState.getStatus();
  }

  /** Gibt die gesamte Spielzeit des Spieles wieder. @return die Spielzeit als long. */
  public long getPlayTimeOfGame() {
    return playTime.getOverallGameDuration();
  }

  /**
   * Gibt die aktuelle Spielzeit zum Zeitpunkt der Abfrage wieder. @return die aktuelle Spielzeit
   * als long.
   */
  public long getPlayActualTime() {
    return playTime.getActualGameDuration();
  }
}
