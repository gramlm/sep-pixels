package level.editor.team.client.view;

import static level.editor.team.client.view.LevelEditorControl.PixelStatus.BLOCK;
import static level.editor.team.client.view.LevelEditorControl.PixelStatus.NONE;
import static level.editor.team.client.view.LevelEditorControl.PixelStatus.START;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.Circle;
import level.editor.team.client.Client;
import level.editor.team.client.model.board.Coordinate;
import level.editor.team.client.model.board.ErzeugeTestLevel;
import level.editor.team.client.model.board.Level;
import level.editor.team.client.model.board.Pixel;
import level.editor.team.client.view.action.PlayLevelEditorMusic;
import level.editor.team.communication.messages.Message;
import level.editor.team.communication.messages.TransferLevel;

/**
 * Klasse steuert den LevelEditorScreen. Sie verwaltet ein GridPane, auf der die Pixel markiert
 * werden, die ein Spieler mit der Maus (MouseOver) auswählt.
 *
 * <p>Zusatzfeatures: - vorgefertigte Level sind im Turnaround verfügbar und können sowohl an den
 * Server zum Spielen gesendet werden als auch verändert oder neu erstellt werden. - GameMusic ist
 * abspielbar
 */
public class LevelEditorControl {

  @FXML Button backToMenuButton;
  @FXML BorderPane borderPane;
  @FXML Arc finish;
  @FXML GridPane gridPane;
  @FXML Button hideLines;
  @FXML Label levelNameAnzeigenLabel;
  @FXML Label levelVorlageNameLabel;
  @FXML Button navigateLevel;
  @FXML TextArea neuerLevelName;
  @FXML Button playMusic;
  @FXML Circle player;
  @FXML Button sendeLevel;
  @FXML Arc start;

  private Client myClient;
  public Level geladenesLevel;
  public Level geladenesLevelDeepCopy;
  private PlayLevelEditorMusic playLevelEditorMusic;
  public List<Level> levelVorlagen = new ArrayList<>();
  int levelNummer = 0;
  boolean gridLinesVisible = true;
  private double blockSize = 8; // Default-Wert

  // hole alle möglichen Level-Koordinaten (d.h. alle des GridPane)
  List<Coordinate> allPossibleCoordinatesOfLevel;

  /**
   * Repräsentiert den Pixel-Status.
   *
   * <ul>
   *   <li>{@code BLOCK}: Pixel ist ein Block.
   *   <li>{@code START}: Pixel ist Start-Element.
   *   <li>{@code FINISH}: Pixel ist Exit-Element.
   *   <li>{@code NONE}: Pixel ist nicht gesetzt.
   * </ul>
   */
  public enum PixelStatus {
    BLOCK,
    START,
    FINISH,
    NONE
  }

  /**
   * Default-Konstruktor des Level-Editors, importiert Test-Level. Die Ursprungskoordinate (0|0) der
   * Levels befindet sich in der linken oberen Ecke.
   */
  public LevelEditorControl() {

    // lade verschiedene Level-Vorlagen
    levelVorlagen.add(new PlayLevelEditorMusic().readLevel("PixelRunner"));
    levelVorlagen.add(new PlayLevelEditorMusic().readLevel("Schacht"));
    levelVorlagen.add(new PlayLevelEditorMusic().readLevel("DontKnowIfItsDifficult"));
    levelVorlagen.add(new PlayLevelEditorMusic().readLevel("EnterCenter"));
    levelVorlagen.add(new PlayLevelEditorMusic().readLevel("Etagen"));
    levelVorlagen.add(new ErzeugeTestLevel().createLevel1());
    levelVorlagen.add(new ErzeugeTestLevel().createLevel2());
    levelVorlagen.add(new ErzeugeTestLevel().createLevel4());

    geladenesLevelDeepCopy = levelVorlagen.get(0);

    playLevelEditorMusic = new PlayLevelEditorMusic();
  }

  @FXML
  void initialize() {

    start.setVisible(false);
    finish.setVisible(false);
    player.setVisible(false);

    navigateLevel.setOnMouseClicked(
        new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent evt) {

            start.setVisible(false);
            finish.setVisible(false);

            // setze die GridPane - Pixel - Blöcke zurück, bevor ein neues Level geladen
            // und gezeichnet wird
            if (geladenesLevel != null) {
              // setze überall einen grauen Hintergrund
              zeichneOderLoescheAlleLevelKoordinaten(true);
              geladenesLevelDeepCopy = getDeepCopyOfLevel(geladenesLevel);
            }

            geladenesLevel = levelVorlagen.get(levelNummer++ % levelVorlagen.size());
            geladenesLevelDeepCopy = getDeepCopyOfLevel(geladenesLevel);
            allPossibleCoordinatesOfLevel = geladenesLevelDeepCopy.getAllCoordinates();
            levelNameAnzeigenLabel.setText(geladenesLevelDeepCopy.getLevelname());

            gridPane.setGridLinesVisible(gridLinesVisible);
            zeichneOderLoescheAlleLevelKoordinaten(false);
          }
        });

    /*
    Wandelt das geladene Level in eine TransferLevel-Message und überträgt es an den Server.
    */
    sendeLevel.setOnMouseClicked(
        new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent event) {

            Message message = null;
            if (geladenesLevel != null) {

              String transferLevelName = geladenesLevelDeepCopy.getLevelname();
              // hole aus TextArea ggf. neuen Level-Namen und benenne das Level um
              if (neuerLevelName.getParagraphs().get(0).toString().length() > 0) {
                transferLevelName = neuerLevelName.getParagraphs().get(0).toString();
              }
              message =
                  getTransferLevelMessage(
                      transferLevelName, convertLevelInStringList(geladenesLevelDeepCopy));
              // schreibe Level als Datei
              new PlayLevelEditorMusic()
                  .writeLevel(transferLevelName, convertLevelInStringList(geladenesLevelDeepCopy));
            } else {
              Alert alert =
                  new Alert(
                      AlertType.INFORMATION,
                      "Bitte erst Level zum Editieren laden, \nindem Du den '>' Button klickst :-)",
                      new ButtonType("OK"));
              alert.showAndWait();
            }

            // sende erzeugtes Level zum Speichern an den Server
            myClient.getClientCommunication().createdNewLevel(message);
          }
        });

    gridPane.setOnMouseClicked(
        new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent event) {

            // bei Klick auf das GridPane wird die Position (column/row) ermittelt
            int columnGridPositionMouse =
                Math.min((int) (event.getX() / blockSize), gridPane.getColumnCount());
            int rowGridPositionMouse =
                Math.min((int) (event.getY() / blockSize), gridPane.getRowCount());

            if (event.getButton().equals(MouseButton.PRIMARY)) {
              // entferne Start-Pixel aus Level-Pixel-Map
              geladenesLevelDeepCopy
                  .getPixels()
                  .remove(
                      Coordinate.of(GridPane.getRowIndex(start), GridPane.getColumnIndex(start)));
              // füge Start-Pixel an der neuen Position hinzu
              geladenesLevelDeepCopy
                  .getPixels()
                  .put(
                      (Coordinate.of(rowGridPositionMouse, columnGridPositionMouse)),
                      new Pixel(false, true, false));
              // setze Start-Pixel an die neue Stelle im GridPane
              GridPane.setConstraints(start, columnGridPositionMouse, rowGridPositionMouse);
              start.setVisible(true);
              addStartOrFinishPixel(columnGridPositionMouse, rowGridPositionMouse);
            } else if (event.getButton().equals(MouseButton.SECONDARY)) {
              // entferne Finish-Pixel aus Level-Pixel-Map
              geladenesLevelDeepCopy
                  .getPixels()
                  .remove(
                      Coordinate.of(GridPane.getRowIndex(finish), GridPane.getColumnIndex(finish)));
              // füge Finish-Pixel an der neuen Position hinzu
              geladenesLevelDeepCopy
                  .getPixels()
                  .put(
                      (Coordinate.of(rowGridPositionMouse, columnGridPositionMouse)),
                      new Pixel(false, false, true));
              // setze Finish-Pixel an die neue Stelle im GridPane
              GridPane.setConstraints(finish, columnGridPositionMouse, rowGridPositionMouse);
              finish.setVisible(true);
              addStartOrFinishPixel(columnGridPositionMouse, rowGridPositionMouse);
            }
          }
        });

    gridPane.setOnMouseMoved(
        new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent event) {

            int columnGridPositionMouse =
                Math.min((int) (event.getX() / blockSize), gridPane.getColumnCount());
            int rowGridPositionMouse =
                Math.min((int) (event.getY() / blockSize), gridPane.getRowCount());

            Label label = new Label("");
            label.setStyle("-fx-border-color: \"grey\"; -fx-border-width: 0.2px;");
            label.setMinWidth(blockSize);
            label.setMinHeight(blockSize);
            label.setAlignment(Pos.CENTER);
            label.setViewOrder(1);

            if (event.isShiftDown()) {
              // füge Block an der neuen Position in der Pixel-TreeMap hinzu
              geladenesLevelDeepCopy
                  .getPixels()
                  .put(
                      (Coordinate.of(rowGridPositionMouse, columnGridPositionMouse)),
                      new Pixel(true, false, false));

              // setze Block an die neue Stelle im GridPane
              label.setBackground(
                  new Background(
                      new BackgroundFill(
                          Color.CADETBLUE, new CornerRadii(0), new Insets(0, 0, 0, 0))));
              gridPane.add(label, columnGridPositionMouse, rowGridPositionMouse);
            }

            if (event.isControlDown()) {
              switch (bestimmePixelTyp(
                  Coordinate.of(rowGridPositionMouse, columnGridPositionMouse),
                  geladenesLevelDeepCopy)) {
                case BLOCK:
                  // entferne Block-Koordinate aus Level-Pixel-Map
                  geladenesLevelDeepCopy
                      .getPixels()
                      .remove(Coordinate.of(rowGridPositionMouse, columnGridPositionMouse));
                  // lösche den Block aus dem GridPane
                  label.setBackground(
                      new Background(
                          new BackgroundFill(
                              Color.WHITE, new CornerRadii(0), new Insets(0, 0, 0, 0))));
                  gridPane.add(label, columnGridPositionMouse, rowGridPositionMouse);
                  break;
                case START:
                  // entferne Start-Pixel aus Level-Pixel-Map
                  geladenesLevelDeepCopy
                      .getPixels()
                      .remove(
                          Coordinate.of(
                              GridPane.getRowIndex(start), GridPane.getColumnIndex(start)));
                  start.setVisible(false);
                  break;
                case FINISH:
                  // entferne Finish-Pixel aus Level-Pixel-Map
                  geladenesLevelDeepCopy
                      .getPixels()
                      .remove(
                          Coordinate.of(
                              GridPane.getRowIndex(finish), GridPane.getColumnIndex(finish)));
                  finish.setVisible(false);
                  break;
                default:
              }
            }
          }
        });

    playMusic.setOnMouseClicked(
        new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent event) {
            /*
            Methode playLevelEditorMusic.playMusicAsCanon()
            spielt die GameMusic im Kanon (Spielhalleneffekt), funktioniert aber nicht ganz
            zuverlässig, deswegen Experimentalstatus und nicht aus dem Projekt gelöscht
            */
            // playLevelEditorMusic.playMusicAsCanon();
            playLevelEditorMusic.loadAndPlayMusic();
          }
        });

    backToMenuButton.setOnMouseClicked(
        new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent event) {
            if (playLevelEditorMusic.getMediaPlayer() != null) {
              playLevelEditorMusic.getMediaPlayer().stop();
            }
            myClient.showMenueScreen();
          }
        });

    hideLines.setOnMouseClicked(
        new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent event) {
            gridLinesVisible = !gridLinesVisible;
            gridPane.setGridLinesVisible(gridLinesVisible);
          }
        });
  }

  private void zeichneOderLoescheAlleLevelKoordinaten(boolean loeschePixel) {

    // hole alle Pixel des Level und deren Koordinaten
    Collection<Coordinate> pixelsOfLevel = geladenesLevelDeepCopy.getPixels().keySet();

    // iteriere über alle Koordinaten und setze das GridPane entsprechend
    for (Coordinate coordinate : pixelsOfLevel) {

      // setze das StartPixel, ExitPixel und den Player
      switch (bestimmePixelTyp(coordinate, geladenesLevelDeepCopy)) {
        case START:
          start.setVisible(!loeschePixel);

          GridPane.setConstraints(start, coordinate.getColumnNumber(), coordinate.getRowNumber());
          // setze den Player ein Pixel rechts neben den Start
          GridPane.setConstraints(
              player, coordinate.getColumnNumber() + 1, coordinate.getRowNumber());
          break;

        case FINISH:
          finish.setVisible(!loeschePixel);
          GridPane.setConstraints(finish, coordinate.getColumnNumber(), coordinate.getRowNumber());
          break;

        case BLOCK:
          Label label = new Label("");
          label.setStyle("-fx-border-color: \"grey\"; -fx-border-width: 0.2px;");
          label.setMinWidth(blockSize);
          label.setMinHeight(blockSize);
          label.setAlignment(Pos.CENTER);
          label.setViewOrder(1);

          if (loeschePixel) {
            label.setStyle("-fx-border-color: f3f3f3; -fx-border-width: 0.2px;");
            label.setBackground(
                new Background(
                    new BackgroundFill(
                        Color.valueOf("f3f3f3"), new CornerRadii(0), new Insets(0, 0, 0, 0))));

          } else {
            label.setBackground(
                new Background(
                    new BackgroundFill(
                        Color.CADETBLUE, new CornerRadii(0), new Insets(0, 0, 0, 0))));
          }
          gridPane.add(label, coordinate.getColumnNumber(), coordinate.getRowNumber());
          break;
        default:
      }
    }
  }

  /**
   * Setzt 5 Pixel an den Start- oder Finish-Block.
   *
   * @param startColumn column des Start-/Finish-Blocks
   * @param startRow row des Start-/Finish-Blocks
   */
  public void addStartOrFinishPixel(int startColumn, int startRow) {
    startColumn = Math.max(startColumn, 2);
    startColumn = Math.min(startColumn, 47);
    startRow = Math.min(startRow, 48);

    for (int column = startColumn - 2; column <= startColumn + 2; column++) {
      geladenesLevelDeepCopy
          .getPixels()
          .put(Coordinate.of(startRow + 1, column), new Pixel(true, false, false));

      Label label = new Label("");
      label.setStyle("-fx-border-color: \"grey\"; -fx-border-width: 0.2px;");
      label.setMinWidth(blockSize);
      label.setMinHeight(blockSize);
      label.setAlignment(Pos.CENTER);
      label.setViewOrder(1);
      label.setBackground(
          new Background(
              new BackgroundFill(Color.CADETBLUE, new CornerRadii(0), new Insets(0, 0, 0, 0))));
      gridPane.add(label, column, startRow + 1);
    }
  }

  private void zeichneAlleGridPaneFelder() {

    // iteriere über alle Koordinaten und setze das GridPane entsprechend
    for (Coordinate coordinate : allPossibleCoordinatesOfLevel) {
      Label label = new Label("");
      label.setStyle("-fx-border-color: \"grey\"; -fx-border-width: 0.2px;");
      label.setMinWidth(blockSize);
      label.setMinHeight(blockSize);
      label.setAlignment(Pos.CENTER);
      label.setViewOrder(1);

      label.setBackground(
          new Background(
              new BackgroundFill(Color.WHITE, new CornerRadii(0), new Insets(0, 0, 0, 0))));
      //  blocks.add(label);

      gridPane.add(label, coordinate.getColumnNumber(), coordinate.getRowNumber());
    }
  }

  /**
   * Konvertiert ein Level in eine String-List.
   *
   * @param level übergebenes Level
   * @return Level als String-List
   */
  public List convertLevelInStringList(Level level) {

    List<String> pixelsOfLevelAlsString = new ArrayList();
    Collection<Coordinate> coordinateCollection = level.getPixels().keySet();

    /*
    String-Format eines Pixels (pixelWords)
    [ row | column | isStartBlock | isFinishBlock | isBlock ]  Separator: ein Leerzeichen
     */

    StringBuilder pixelWord = new StringBuilder();

    // iteriere über alle Koordinaten, prüfe welcher Pixel-Typ und wandle in ein Pixel-Word
    for (Coordinate coordinate : coordinateCollection) {
      switch (bestimmePixelTyp(coordinate, level)) {
        case BLOCK:
          pixelWord
              .append(coordinate.getRowNumber())
              .append(" ")
              .append(coordinate.getColumnNumber())
              .append(" ")
              .append(false)
              .append(" ")
              .append(false)
              .append(" ")
              .append(true);
          break;
        case START:
          pixelWord
              .append(coordinate.getRowNumber())
              .append(" ")
              .append(coordinate.getColumnNumber())
              .append(" ")
              .append(true)
              .append(" ")
              .append(false)
              .append(" ")
              .append(false);
          break;
        case FINISH:
          pixelWord
              .append(coordinate.getRowNumber())
              .append(" ")
              .append(coordinate.getColumnNumber())
              .append(" ")
              .append(false)
              .append(" ")
              .append(true)
              .append(" ")
              .append(false);
          break;
        default:
      }

      pixelsOfLevelAlsString.add(new String(pixelWord));
      pixelWord = new StringBuilder();
    }
    // zum Format siehe Connection-Klasse in communication

    return pixelsOfLevelAlsString;
  }

  private PixelStatus bestimmePixelTyp(Coordinate coordinate, Level level) {

    if (level.getPixels().containsKey(coordinate)) {
      if (level.getPixel(coordinate).isBlock()) {
        return BLOCK;
      }
      if (level.getPixel(coordinate).isStart()) {
        return START;
      }
      if (level.getPixel(coordinate).isFinish()) {
        return PixelStatus.FINISH;
      }
    }
    return NONE;
  }

  /**
   * Erstellt eine Deep-Copy eines Levels, welches verändert werden kann.
   *
   * @param level übergebenes Level
   * @return Deep-Level-Copy
   */
  private Level getDeepCopyOfLevel(Level level) {
    Message message =
        getTransferLevelMessage(level.getLevelname(), convertLevelInStringList(level));
    return new Level(message);
  }

  public void setMyClient(Client myClient) {
    this.myClient = myClient;
  }

  /*
  Methode ist nur für den LevelEditor-Test da.
  */
  public Message getTransferLevelMessage(String levelName, List<String> levelString) {
    return new TransferLevel(levelName, levelString);
  }
}
