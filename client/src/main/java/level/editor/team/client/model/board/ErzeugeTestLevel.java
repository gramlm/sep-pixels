package level.editor.team.client.model.board;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import level.editor.team.communication.messages.Message;
import level.editor.team.communication.messages.TransferLevel;

/**
 * Klasse dient der Erzeugung von Test-Leveln.
 * Methoden sind teilweise übernommen und adaptiert aus dem LevelManager.
 */
public class ErzeugeTestLevel {

  /**
   * Level als Moshi-String erzeugen, welches lediglich aus dem Start, dem Ausgang (Finish) und den
   * Blöcken in einer Reihe besteht. Ein Objekt besteht aus den Integerwerten der Koordinate (Zeile,
   * Spalte) und den Boolean-Werten für isStart, isFinish, isBlock.
   */
  public Level createLevel1() {
    // ggf. muss der Name des Levels hier festgelegt werden und ist nicht als Parameter zu übergeben

    List<String> testLevel = new ArrayList<>();
    StringBuilder builder = new StringBuilder();
    String levelObject;

    // Start einfügen
    levelObject =
        builder
            .append(39)
            .append(" ")
            .append(7)
            .append(" ")
            .append(true)
            .append(" ")
            .append(false)
            .append(" ")
            .append(false)
            .toString();
    testLevel.add(levelObject);
    builder.delete(0, builder.length());

    // Finish einfügen
    levelObject =
        builder
            .append(39)
            .append(" ")
            .append(41)
            .append(" ")
            .append(false)
            .append(" ")
            .append(true)
            .append(" ")
            .append(false)
            .toString();
    testLevel.add(levelObject);
    builder.delete(0, builder.length());

    // Blöcke einfügen
    for (int i = 3; i < 45; i++) {
      levelObject =
          builder
              .append(40)
              .append(" ")
              .append(i)
              .append(" ")
              .append(false)
              .append(" ")
              .append(false)
              .append(" ")
              .append(true)
              .toString();
      testLevel.add(levelObject);
      builder.delete(0, builder.length());
    }
    // writeLevel(testLevel);
    return new Level(new TransferLevel("Gerades Level", testLevel));
  }

  /**
   * Level als Moshi-String erzeugen, welches lediglich aus dem Start, dem Ausgang (Finish) und den
   * Blöcken in einer Reihe besteht. Ein Objekt besteht aus den Integerwerten der Koordinate (Zeile,
   * Spalte) und den Boolean-Werten für isStart, isFinish, isBlock.
   */
  public Level createLevel2() {
    // ggf. muss der Name des Levels hier festgelegt werden und ist nicht als Parameter zu übergeben

    List<String> testLevel = new ArrayList<>();
    StringBuilder builder = new StringBuilder();
    String levelObject;

    // Start einfügen
    levelObject =
        builder
            .append(39)
            .append(" ")
            .append(7)
            .append(" ")
            .append(true)
            .append(" ")
            .append(false)
            .append(" ")
            .append(false)
            .toString();
    testLevel.add(levelObject);
    builder.delete(0, builder.length());

    // Finish einfügen
    levelObject =
        builder
            .append(24)
            .append(" ")
            .append(47)
            .append(" ")
            .append(false)
            .append(" ")
            .append(true)
            .append(" ")
            .append(false)
            .toString();
    testLevel.add(levelObject);
    builder.delete(0, builder.length());

    // Blöcke einfügen
    for (int i = 3; i < 15; i++) {
      levelObject =
          builder
              .append(40)
              .append(" ")
              .append(i)
              .append(" ")
              .append(false)
              .append(" ")
              .append(false)
              .append(" ")
              .append(true)
              .toString();
      testLevel.add(levelObject);
      builder.delete(0, builder.length());
    }

    // Blöcke einfügen
    for (int i = 13; i < 25; i++) {
      levelObject =
          builder
              .append(35)
              .append(" ")
              .append(i)
              .append(" ")
              .append(false)
              .append(" ")
              .append(false)
              .append(" ")
              .append(true)
              .toString();
      testLevel.add(levelObject);
      builder.delete(0, builder.length());
    }

    // Blöcke einfügen
    for (int i = 27; i < 32; i++) {
      levelObject =
          builder
              .append(31)
              .append(" ")
              .append(i)
              .append(" ")
              .append(false)
              .append(" ")
              .append(false)
              .append(" ")
              .append(true)
              .toString();
      testLevel.add(levelObject);
      builder.delete(0, builder.length());
    }

    // Blöcke einfügen
    for (int i = 35; i < 40; i++) {
      levelObject =
          builder
              .append(27)
              .append(" ")
              .append(i)
              .append(" ")
              .append(false)
              .append(" ")
              .append(false)
              .append(" ")
              .append(true)
              .toString();
      testLevel.add(levelObject);
      builder.delete(0, builder.length());
    }

    // Blöcke einfügen
    for (int i = 40; i < 50; i++) {
      levelObject =
          builder
              .append(25)
              .append(" ")
              .append(i)
              .append(" ")
              .append(false)
              .append(" ")
              .append(false)
              .append(" ")
              .append(true)
              .toString();
      testLevel.add(levelObject);
      builder.delete(0, builder.length());
    }
    return new Level(new TransferLevel("Sprung-Level", testLevel));
  }

  /**
   * Test-Level 3.
   *
   * @return Test-Level 3
   */
  public Level createLevel3() {
    Message leeresLevel = new TransferLevel("Parabel", new ArrayList<>());
    Level leeresLevelVorlage = new Level(leeresLevel);

    // Level verändern
    Map map = leeresLevelVorlage.getPixels();
    for (int i = 5; i < 30; i++) {
      // verschiedene Level-Erzeugungsexperimente
      //  map.put(Coordinate.of(i, i), new Pixel(true, false, false));
      //  map.put(Coordinate.of(i+1, i+1), new Pixel(true, false, false));
      map.put(Coordinate.of((i * i / 50) + 5, i), new Pixel(true, false, false));
    }
    for (int i = 35; i < 50; i++) {
      //  map.put(Coordinate.of(i, i), new Pixel(true, false, false));
      //  map.put(Coordinate.of(i+1, i+1), new Pixel(true, false, false));
      map.put(Coordinate.of(35, i), new Pixel(true, false, false));
    }
    return leeresLevelVorlage;
  }

  /**
   * Erzeugt ein leeres Level zum Editieren.
   *
   * @return Level
   */
  public Level createLevel4() {
    Message leeresLevel = new TransferLevel("Leeres Level", new ArrayList<String>());
    return new Level(leeresLevel);
  }
}
