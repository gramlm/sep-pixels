package level.editor.team.client.view;

import java.util.List;
import java.util.Map;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import level.editor.team.client.Client;

/** Klasse steuert den MenuScreenControl. */
public class MenuScreenControl {
  @FXML private ChoiceBox levelDropDown;
  @FXML private Button normaloButton;
  @FXML private Button superJumperButton;
  @FXML private Button superRunnerButton;
  @FXML private Button startGameButton;
  @FXML private Label commentLabel;

  @FXML private TableView<LevelStatisticClient> statisticTable;
  @FXML private TableColumn<LevelStatisticClient, String> levelName;
  @FXML private TableColumn<LevelStatisticClient, String> besteZeit;
  @FXML private TableColumn<LevelStatisticClient, String> versuche;
  @FXML private TableColumn<LevelStatisticClient, String> gelungeneVersuche;
  @FXML private TableColumn<LevelStatisticClient, String> spielerNameBesteZeit;

  private Client client;
  private String selectedLevel;

  public void setMyClient(Client myClient) {
    this.client = myClient;
  }

  @FXML
  void initialize() {
    String dropDownText = "Level-level-nivel-niveau";
    levelDropDown.setValue(dropDownText);
    levelDropDown.setStyle("-fx-font-size: 18px; ");

    // Verbindet die Zellen mit der Statistic
    // https://stackoverflow.com/questions/53579307/how-to-get-content-of-an-arraylist-to-show-in-javafx-gui

    levelName.setCellValueFactory(new PropertyValueFactory<>("levelName"));
    besteZeit.setCellValueFactory(new PropertyValueFactory<>("besteZeit"));
    versuche.setCellValueFactory(new PropertyValueFactory<>("versuche"));
    gelungeneVersuche.setCellValueFactory(new PropertyValueFactory<>("gelungeneVersuche"));
    spielerNameBesteZeit.setCellValueFactory(new PropertyValueFactory<>("spielerNameBesteZeit"));
  }

  @FXML
  private void loadLevel(ActionEvent event) {
    // MouseEventHandler zum Anklicken der einzelnen LevelButton
    if (levelDropDown.getItems().size() == 0) {
      return;
    }
    if (levelDropDown.getValue() == null) {
      return;
    }
    selectedLevel = (String) levelDropDown.getValue();
    commentLabel.setText("");
  }

  @FXML
  private void loadLevelEditor(ActionEvent event) {
    client.showEditorScreen();
  }

  // Zusatzfeature: Auswahl eines Spielers
  @FXML
  private void chooseNormalo() {
    client.setFigure("normalo");
    commentLabel.setText("");
    normaloButton.setStyle("-fx-border-color: #ffc71f; -fx-border-width: 3px");
    superJumperButton.setStyle("-fx-border-color: \"grey\"; -fx-border-width: 1px");
    superRunnerButton.setStyle("-fx-border-color: \"grey\"; -fx-border-width: 1px");
  }

  @FXML
  private void chooseSuperJumper() {
    client.setFigure("superJumper");
    commentLabel.setText("");
    superJumperButton.setStyle("-fx-border-color: #21b8ee; -fx-border-width: 3px");
    normaloButton.setStyle("-fx-border-color: \"grey\"; -fx-border-width: 1px");
    superRunnerButton.setStyle("-fx-border-color: \"grey\"; -fx-border-width: 1px");
  }

  @FXML
  private void chooseSuperRunner() {
    client.setFigure("superRunner");
    commentLabel.setText("");
    superRunnerButton.setStyle("-fx-border-color: #d9257d; -fx-border-width: 3px");
    normaloButton.setStyle("-fx-border-color: \"grey\"; -fx-border-width: 1px");
    superJumperButton.setStyle("-fx-border-color: \"grey\"; -fx-border-width: 1px");
  }

  @FXML
  private void startGame() {
    if (selectedLevel != null && client.getFigure() != null) {
      client.playGame(selectedLevel);
    } else if (selectedLevel == null && client.getFigure() != null) {
      commentLabel.setText(client.getActiveBundle().getString("MenuScreen.selectLevel"));
    } else if (selectedLevel != null) {
      commentLabel.setText(client.getActiveBundle().getString("MenuScreen.selectPlayer"));
    } else {
      commentLabel.setText(client.getActiveBundle().getString("MenuScreen.selectPlayerAndLevel"));
    }
  }

  /** Das DrpoDown-Menü zum Level-Auswählen wird gefüllt. */
  public void showDropDownMenue(List<String> levels) {
    levelDropDown.getItems().clear();
    if (levels != null) {
      levelDropDown.getItems().addAll(levels);
    }
  }

  /** Die Tabelle mit den statistischen Informationen wird gefüllt. */
  public void showStatisticTable(Map<String, List<String>> statisticMap) {
    for (String levels : statisticMap.keySet()) {
      if (statisticMap.get(levels).get(1).equals("0")) {
        statisticTable
            .getItems()
            .add(
                new LevelStatisticClient(
                    levels,
                    "--",
                    statisticMap.get(levels).get(2),
                    statisticMap.get(levels).get(1),
                    statisticMap.get(levels).get(3)));
      } else {
        statisticTable
            .getItems()
            .add(
                new LevelStatisticClient(
                    levels,
                    String.valueOf(Double.parseDouble(statisticMap.get(levels).get(0)) / 1000)
                        + " s",
                    statisticMap.get(levels).get(2),
                    statisticMap.get(levels).get(1),
                    statisticMap.get(levels).get(3)));
      }
    }
  }
}
