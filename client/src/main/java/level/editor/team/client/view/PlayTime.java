package level.editor.team.client.view;

import java.time.Duration;
import java.time.Instant;

/**
 * Klasse erzeugt eine Zeitanzeige, die dann im #Gamescreen im @gameTimeLabel angezeigt wird und
 * schließlich in der Levelstatistik gespeichert wird.
 */
public class PlayTime {

  // https://docs.oracle.com/javase/8/docs/api/java/time/Instant.html
  // Startzeit

  private Instant startTime;
  private boolean isRunning;

  public void start() {
    startTime = Instant.now();
  }

  // Spieldauer wird ausgerechnet während des Spiels

  public long getActualGameDuration() {
    return Duration.between(startTime, Instant.now()).toMillis();
  }

  // Endzeit

  private Instant endTime;

  public void stop() {
    endTime = Instant.now();
  }

  // Gesamtspielzeit wird ausgerechnet

  public long getOverallGameDuration() {
    return Duration.between(startTime, endTime).toMillis();
  }

  // hier kann man versuchen, sein persönliches Zeitlimit zu untertreffen

  public boolean isInMaxDuration(Duration myTimeLimit) {
    return Instant.now().isBefore((Instant) myTimeLimit.addTo(startTime));
  }

  public boolean isRunning() {
    return isRunning;
  }
}
