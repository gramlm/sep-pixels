package level.editor.team.client.model.board;

import java.util.Objects;

/**
 * Klasse repäsentiert die Position eines Pixels in einem Level. Code orientiert an Task2-solution
 */
public final class Coordinate implements Comparable<Coordinate> {

  private final int row;
  private final int column;

  private Coordinate(int row, int column) {
    this.row = row;
    this.column = column;
  }

  /** Erzeugt eine neue Koordinate. */
  public static Coordinate of(int row, int column) {
    // es werden nur Koordinaten im Bereich des GridPane zugelassen
    row = Math.max(0, row);
    row = Math.min(49, row);
    column = Math.max(0, column);
    column = Math.min(49, column);
    return new Coordinate(row, column);
  }

  public int getRowNumber() {
    return row;
  }

  public int getColumnNumber() {
    return column;
  }

  @Override
  public int compareTo(Coordinate o) {
    if (getRowNumber() != o.getRowNumber()) {
      return getRowNumber() - o.getRowNumber();
    } else {
      return getColumnNumber() - o.getColumnNumber();
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Coordinate that = (Coordinate) o;
    return row == that.row && column == that.column;
  }

  @Override
  public int hashCode() {
    return Objects.hash(row, column);
  }

  @Override
  public String toString() {
    return "Koordinate: (" + row + " | " + column + ")";
  }
}
