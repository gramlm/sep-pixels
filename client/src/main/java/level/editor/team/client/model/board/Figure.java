package level.editor.team.client.model.board;

import javafx.scene.paint.Color;

/** Klasse repäsentiert die Spielfigur des Spiels. */
public class Figure {

  private final double runVelocity;
  private final double jumpVelocity;
  private final Color color;

  /**
   * Konstruktor für eine neue Figur, die eine Lauf- und eine Sprunggeschwindigkeit hat sowie eine
   * Farbe.
   */
  public Figure(double runVelocity, double jumpVelocity, Color color) {
    this.runVelocity = runVelocity;
    this.jumpVelocity = jumpVelocity;
    this.color = color;
  }

  public double getRunVelocity() {
    return runVelocity;
  }

  public double getJumpVelocity() {
    return jumpVelocity;
  }

  public Color getColor() {
    return color;
  }
}
