package level.editor.team.client.view.action;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import level.editor.team.client.model.board.Level;
import level.editor.team.communication.messages.TransferLevel;

/** Spielt die GameMusic ab. */
public class PlayLevelEditorMusic {

  Media media;
  MediaPlayer mediaPlayer;

  /** Lädt und spielt die Musikdatei. */
  public void loadAndPlayMusic() {
    String path =
        PlayLevelEditorMusic.class.getResource("/ArnheitersPixelRunnerMusic.mp3").toString();
    this.media = new Media(path);
    this.mediaPlayer = new MediaPlayer(media);
    mediaPlayer.play();
  }

  /**
   * GameMusic soll als Kanon im Abstand einer Sekunde in einem neuen Thread gestartet werden.
   * Experimentalstatus... s. dazu Kommentar in GameMusicThreadExperiment
   */
  public void playMusicAsCanon() {
    /*
    new Thread(
            new Runnable() {
              @Override
              public void run() {
                String path3 =
                    PlayLevelEditorMusic.class
                        .getResource("/ArnheitersPixelRunnerMusic.mp3")
                        .toString();
                Media media3 = new Media(path3);
                MediaPlayer mediaPlayer3 = new MediaPlayer(media3);
                mediaPlayer3.play();
              }
            })
        .start();


    */

    new GameMusicThreadExperiment().start();

    String path2 =
        PlayLevelEditorMusic.class.getResource("/ArnheitersPixelRunnerMusic.mp3").toString();
    Media media2 = new Media(path2);
    MediaPlayer mediaPlayer2 = new MediaPlayer(media2);
    mediaPlayer2.play();

    try {
      Thread.sleep(1025);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public void stopMusic() {
    mediaPlayer.stop();
  }

  public MediaPlayer getMediaPlayer() {
    return mediaPlayer;
  }

  /**
   * Schreibt ein Level in den Ordner SavedLevels.
   *
   * <p>Die folgenden beiden Methoden passen eigentlich nicht in die Klasse, gehören aber
   * zum Editor und wurden später aus möglichen Bug-Gründen nicht mehr in eine andere
   * Klasse ausgelagert.
   *
   * <p>Quelle: angepasster Code aus dem Server
   *
   * @param newLevel das zu schreibende Level als String-Liste.
   */
  public synchronized void writeLevel(String levelName, List<String> newLevel) {

    File file = new File("src/main/resources/SavedLevels" + File.separator);

    try {
      file = new File(file.getAbsolutePath(), levelName + ".txt");
      BufferedWriter bw = new BufferedWriter(new FileWriter(file));
      for (String zeile : newLevel) {
        bw.write(zeile);
        bw.newLine();
      }
      bw.close();
    } catch (IOException e) {
      System.out.println("An error occurred.");
      e.printStackTrace();
    }
  }

  /**
   * Liest eine gespeicherte Level-Datei und erzeugt ein Level daraus.
   *
   * <p>Quelle: angepasster Code aus dem Server
   *
   * @param levelName zu lesender LevelName bzw. Dateiname (.txt wird angehängt)
   * @return Level
   */
  public Level readLevel(String levelName) {
    File file = new File("src/main/resources/SavedLevels" + File.separator);

    List newLevelStringList = new ArrayList<String>();

    try {
      file = new File(file.getAbsolutePath(), levelName + ".txt");
      BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

      String readLine = bufferedReader.readLine();

      while (readLine != null) {
        newLevelStringList.add(readLine);
        readLine = bufferedReader.readLine();
      }
      bufferedReader.close();

    } catch (IOException e) {
      System.out.println("An error occurred.");
      e.printStackTrace();
    }

    return new Level(new TransferLevel(levelName, newLevelStringList));
  }
}
