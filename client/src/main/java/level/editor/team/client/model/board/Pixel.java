package level.editor.team.client.model.board;

/**
 * Klasse repäsentiert eine Zelle, die immer nur aus einem einzigen Pixel besteht und ggf. den
 * Block, den Eingang oder den Ausgang enthalten kann.
 */
public class Pixel {

  private final boolean isBlock;
  private final boolean isStart;
  private final boolean isFinish;

  /**
   * Konstruktor für ein Pixel.
   *
   * @param isBlock Boolean für Blöcke
   * @param isStart Boolean für den Eingang
   * @param isFinish Boolean für den Ausgang
   */
  public Pixel(boolean isBlock, boolean isStart, boolean isFinish) {
    this.isBlock = isBlock;
    this.isStart = isStart;
    this.isFinish = isFinish;
  }

  public boolean isBlock() {
    return isBlock;
  }

  public boolean isFinish() {
    return isFinish;
  }

  public boolean isStart() {
    return isStart;
  }
}
