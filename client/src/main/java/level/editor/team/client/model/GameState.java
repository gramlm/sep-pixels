package level.editor.team.client.model;

import level.editor.team.client.model.board.Level;
import level.editor.team.client.view.PlayTime;

/**
 * Klasse speichert den aktuellen Spielzustand, einschließlich GameStatus (Ongoing, Gameover),
 * Level, Spielername. Diese Klasse wurde nach dem Vorbild der Lösungen von Task-3 (HighLowCardGame)
 * und Task-4 (Bauernschach) erstellt. https://uni2work.ifi.lmu.de/course/W21/IfI/PEEGS-LNQ/file
 */
public class GameState {

  /** Gamestatus wird definiert. */
  public enum GameStatus {
    ONGOING,
    GAME_OVER,
    WON
  }

  private GameStatus gameStatus;
  private String playerName;
  private Level level;

  /**
   * Konstruktor des GamesState.
   *
   * @param playerName der Name des Spielers als String.
   * @param level das zu spielende Level.
   */
  public GameState(String playerName, Level level) {
    gameStatus = GameStatus.ONGOING;
    this.playerName = playerName;
    this.level = level;
  }

  /**
   * Abfrage über Spielstatus.
   *
   * <p>@return boolean Status
   */
  public boolean isGameRunning() {
    if (gameStatus.equals(GameStatus.ONGOING)) {
      return true;
    } else {
      return false;
    }
  }

  public void setGameIsWon() {
    gameStatus = GameStatus.WON;
  }

  public void setGameIsOver() {
    gameStatus = GameStatus.GAME_OVER;
  }

  public GameStatus getStatus() {
    return gameStatus;
  }

  /**
   * Abfrage über Spielstatus.
   *
   * <p>@return boolean Status
   */
  public boolean isGameWon() {
    if (gameStatus == GameStatus.WON) {
      return true;
    } else {
      return false;
    }
  }

  public Level getLevel() {
    return level;
  }
}
