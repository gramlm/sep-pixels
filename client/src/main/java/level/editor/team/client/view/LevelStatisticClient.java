package level.editor.team.client.view;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.StringPropertyBase;

/**
 * Diese Klasse repräsentiert die Statistik für jedes Level. Sie wird für die TableView benötigt.
 */
public class LevelStatisticClient {

  private final StringProperty levelName;
  private final StringProperty besteZeit;
  private final StringProperty versuche;
  private final StringProperty gelungeneVersuche;
  private final StringProperty spielerNameBesteZeit;

  /**
   * Konstruktor der LevelStatistic des Clients.
   *
   * @param levelName Der LevelName
   * @param besteZeit Die Bestzeit des Levels
   * @param versuche Die Anzahl der insgesamt getätigten Versuche.
   * @param gelungeneVersuche Die Anzahl der gelungenen Versuche.
   * @param spielerNameBesteZeit Der Name des schnellsten Spielers.
   */
  public LevelStatisticClient(
      String levelName,
      String besteZeit,
      String versuche,
      String gelungeneVersuche,
      String spielerNameBesteZeit) {
    this.levelName = new SimpleStringProperty(levelName);
    if (besteZeit.equals(String.valueOf(Double.POSITIVE_INFINITY))) {
      this.besteZeit = new SimpleStringProperty("---");
    } else {
      this.besteZeit = new SimpleStringProperty(besteZeit);
    }
    this.versuche = new SimpleStringProperty(versuche);
    this.gelungeneVersuche = new SimpleStringProperty(gelungeneVersuche);
    this.spielerNameBesteZeit = new SimpleStringProperty(spielerNameBesteZeit);
  }

  public String getLevelName() {
    return levelName.get();
  }

  public String getBesteZeit() {
    return besteZeit.get();
  }

  public String getVersuche() {
    return versuche.get();
  }

  public String getGelungeneVersuche() {
    return gelungeneVersuche.get();
  }

  public String getSpielerNameBesteZeit() {
    return spielerNameBesteZeit.get();
  }
}
